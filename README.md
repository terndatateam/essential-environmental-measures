# The Database
This app is configured to use a cloud Postgres 9.6 instance. Everything is pre-configured, except for the password. You need to supply this password when you run the spring-boot app.

### command line
If you run from the command line/terminal, use:
```bash
./mvnw spring-boot:run -D--spring.datasource.password=<thepassword> # replace with the password
```

### SpringSource Tool Suite
If you run from Eclipse, you can define `Override Properties` for your run configuration. See [this blog](https://spring.io/blog/2015/03/18/spring-boot-support-in-spring-tool-suite-3-6-4#running-a-boot-app-in-sts) and find "Override Properties" in the page if you get lost for how to configure these.

### something else
If you run the app a different way, you need to figure out how to provide the `spring.datasource.password` value. You might find some hints in the doco: https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html.

## schema policy
If the database is empty when the app is run, it will create all the tables it requires.

If the database already has the tables, they will be truncated.

This means it's safe to keep re-running the app over and over.

If you run into any issues, just delete all the tables in the database and let the app re-create them. This is only likely to happen when you change the model classes.

## connecting to the database for queries by hand
The easiest way to run queries against the database is using pgAdmin3. You can install that on Ubuntu with:
```bash
sudo apt-get install pgadmin3
```

Then run it by running the `pgadmin3` command in a terminal or find it in the applications menu.

You then need to add a database to connect to with the following values:
```
name=eem
host=beast.ecoinformatics.org.au
port=5434
maintenance db=postgres
username=squid
password=<insert password here>
```

The `squid-eem` database is the one that the app uses. Run all your queries against that.


## Running a database locally using Docker
Start Postgres with:
```bash
docker run \
  -e POSTGRES_USER=docker \
  -e POSTGRES_PASSWORD=docker \
  -e POSTGRES_DB=docker \
  -p 5432:5432 \
  --detach \
  --name=eem-pg \
  postgres:10
```

Then run the app with:
```bash
./mvnw spring-boot:run \
  -D--spring.datasource.password=docker \
  -D--spring.datasource.url=jdbc:postgresql://localhost:5432/docker \
  -D--spring.datasource.username=docker
```

## Deploying to Heroku

TODO talk about deploying the app itself. Hint, just follow the Heroku guide for a spring-boot app.

### Database
This app wants to create and populate the DB on startup, which doesn't work well for Heroku.

Our workaround for that is to disable the DB population when running on Heroku (run `heroku config:set SKIP_POPULATE=1`) and restore a DB backup, that you produce from a local instance, to the Heroku DB:

  1. run the app locally
  1. dump the Postgres DB using `custom` format, let's assume we save it to `/tmp/eem.dump`
  1. the dump needs to be publicly accessible on the net so we'll use S3, make sure you have `awscli` configured
  1. copy the dump to S3: `aws s3 cp /tmp/eem.dump s3://somebucket/`
  1. generate a presigned URL: `aws s3 presign s3://somebucket/eem.dump`, it will look something like: https://somebucket.s3.amazonaws.com/eem.dump?Signature=<somesig>&Expires=1525418423&AWSAccessKeyId=<somekey>
  1. trigger the DB restore with (insert your URL before running): `heroku pg:backups:restore '<insert presigned URL from above>' DATABASE_URL`
