#!/usr/bin/python3
import requests
# make sure you've run `pip install requests[socks]` to install socks proxy support
import sys
from datetime import datetime

# host = 'localhost:9999'
host = 'aekosnep.cfpwskzydg0w.us-east-1-beta.rds.amazonaws.com:8182'
print('Running against host %s' % host)

query = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX aekos: <http://www.aekos.org.au/ontology/1.0.0#>
PREFIX api: <http://www.aekos.org.au/api/1.0#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

PREFIX abares_fgc: <http://www.aekos.org.au/ontology/1.0.0/abares_fgc#>
PREFIX adelaide_koonamore: <http://www.aekos.org.au/ontology/1.0.0/adelaide_koonamore#>
PREFIX aekos_common: <http://www.aekos.org.au/ontology/1.0.0/aekos_common#>
PREFIX dewnr_bdbsa: <http://www.aekos.org.au/ontology/1.0.0/dewnr_bdbsa#>
PREFIX dewnr_roadsideveg: <http://www.aekos.org.au/ontology/1.0.0/dewnr_roadsideveg#>
PREFIX dpipwe_platypus: <http://www.aekos.org.au/ontology/1.0.0/dpipwe_platypus#>
PREFIX oeh_vis: <http://www.aekos.org.au/ontology/1.0.0/oeh_vis#>
PREFIX qld_corveg: <http://www.aekos.org.au/ontology/1.0.0/qld_corveg#>
PREFIX tern_ausplots: <http://www.aekos.org.au/ontology/1.0.0/tern_ausplots#>
PREFIX tern_swatt: <http://www.aekos.org.au/ontology/1.0.0/tern_swatt#>
PREFIX tern_trend: <http://www.aekos.org.au/ontology/1.0.0/tern_trend#>
PREFIX uq_supersites_cover: <http://www.aekos.org.au/ontology/1.0.0/uq_supersites_cover#>
PREFIX usyd_derg: <http://www.aekos.org.au/ontology/1.0.0/usyd_derg#>
PREFIX wadec_ravensthorpe: <http://www.aekos.org.au/ontology/1.0.0/wadec_ravensthorpe#>

SELECT
  ?featureOfInterestID 
	?feature
	?featureQualifier
	?originalFeatureType
	?sampleUnit
	?method
	?locationID
	?characteristic
	?characteristicQualifier
	?protocolLink
	?dateTime
	?value
	?rangelow
	?rangehigh
	?standard
	?category
	?comment
	?obsit 

FROM <http://www.aekos.org.au/ontology/1.0.0/aekos_common#>
FROM <http://www.aekos.org.au/ontology/1.0.0/tern_ausplots#>

WHERE {
  BIND ("dummy" as ?originalFeatureType) .
  ?slsg a aekos:STUDYLOCATIONSUBGRAPH .
  ?slsg aekos:aekoslocationidentifier ?locationID .
  ?slsg aekos:views ?viewsBag .
  ?viewsBag ?viewsBagProp ?viewsItems FILTER(?viewsBagProp != rdf:type) .
  ?viewsItems aekos:observeditems ?obsBag .
  ?obsBag ?obsBagProp ?obsit FILTER(?obsBagProp != rdf:type) .
  ?obsit a ?orggroups .
  VALUES ?orggroups {
    aekos:INDIVIDUALORGANISM
    aekos:ORGANISMASSEMBLAGE
    aekos:SPECIESORGANISMGROUP
  }
  BIND(?obsit as ?featureOfInterestID)
  # Populate the connections to sampling context
  ?obsit aekos:featureof ?sampleUnit .
  ?obsit aekos:temporality/aekos:has_start ?dateTime .
  ?obsit aekos:method ?method .
  ?method aekos:method ?protocolLink .
  # Get and populate average height values
  OPTIONAL {
  	?obsit aekos:averageheight ?avgHeight .
	  BIND ("true" as ?heightDataCheck) .  # Flag that height data exists
    BIND ("AVG" as ?charQualAveHeight) .
    OPTIONAL {?avgHeight aekos:value ?avhtval .}
    OPTIONAL {
      ?avgHeight aekos:range ?avhtrange .
      OPTIONAL{?avhtrange aekos:lowervalue ?avhtrangelow .}
      OPTIONAL{?avhtrange aekos:uppervalue ?avhtrangehigh .}    
    }
    OPTIONAL {?avgHeight aekos:units/aekos:name ?avhtunit .}
    OPTIONAL {?avgHeight aekos:category ?avhtcat .}
	  OPTIONAL {?avgHeight aekos:commentary ?avhtcomment .}
  }
  # Get and populate height values
  OPTIONAL {
    ?obsit aekos:height ?height .
  	BIND ("true" as ?heightDataCheck) .  # Flag that height data exists
    BIND (?nothing as ?charQualHeight) .
    OPTIONAL {?height aekos:value ?htval .}
    OPTIONAL {?height aekos:range ?htrange .
      	OPTIONAL{?htrange aekos:lowervalue ?htrangelow .}
      	OPTIONAL{?htrange aekos:uppervalue ?htrangehigh .}
    } 
    OPTIONAL {?height aekos:units/aekos:name ?htunit .}
    OPTIONAL {?height aekos:category ?htcat .}
    OPTIONAL {?height aekos:commentary ?htcomment .}
  }
  # Filter on records with height data
  FILTER (bound(?heightDataCheck)) .
  BIND (if(bound(?heightDataCheck),"Height",?nothing) as ?charHt)
  BIND (COALESCE(?charHt, ?charName, ?ddd) as ?characteristic) .
  BIND (COALESCE(?charQualHeight,?charQualAveHeight, ?ddd) as ?characteristicQualifier) .
  BIND (COALESCE(?avhtval, ?htval, ?nothing) as ?value) .
  BIND (COALESCE(?avhtrangelow, ?htrangelow, ?nothing) as ?rangelow) .
  BIND (COALESCE(?avhtrangehigh, ?htrangehigh, ?nothing) as ?rangehigh) .
  BIND (COALESCE(?avhtunit, ?htunit, ?nothing) as ?standard) .
  BIND (COALESCE(?avhtcat, ?htcat, ?nothing) as ?category) .
  BIND (COALESCE(?avhtcomment, ?htcomment, ?nothing) as ?comment) .

  # Populate the feature of interest and qualifier with correct terms
  BIND (if(sameTerm(?orggroups, aekos:INDIVIDUALORGANISM), "BioticItem",?nothing) as ?featInd) .
  BIND (if(sameTerm(?orggroups, aekos:INDIVIDUALORGANISM), "Individual",?nothing) as ?featIndQual) .
  BIND (if(sameTerm(?orggroups, aekos:ORGANISMASSEMBLAGE), "AssemblageOrgGroup",?nothing) as ?featAssem) .
  BIND (if(sameTerm(?orggroups, aekos:ORGANISMASSEMBLAGE), "Stand",?nothing) as ?featAssemQual) .
  BIND (if(sameTerm(?orggroups, aekos:SPECIESORGANISMGROUP), "SpeciesOrgGroup",?nothing) as ?featSpecies) .
  BIND (if(sameTerm(?orggroups, aekos:SPECIESORGANISMGROUP), "Stand",?nothing) as ?featSpeciesQual) .
  BIND (COALESCE(?featInd, ?featAssem, ?featSpecies) as ?feature) .
  BIND (COALESCE(?featIndQual, ?featAssemQual, ?featSpeciesQual) as ?featureQualifier) .  
}
"""

def now():
    return datetime.now().timestamp()


def nextpage(total_elapsed, offset, page_size):
    headers = {
        'Accept': 'application/json'
    }
    payload = {
        'query': '''
            %s
            LIMIT %d
            OFFSET %d
        ''' % (query, page_size, offset)
    }
    # you should have a SSH socks5 proxy running with a command like:
    #   ssh -nNC -D 9999 ec2-user@<EC2 IP>
    proxies = {
        'http': 'socks5://localhost:9999',
        'https': 'socks5://localhost:9999',
    }
    start = now()
    resp = requests.get('http://%s/sparql' % host, headers=headers, params=payload, proxies=proxies)
    elapsed = (now() - start) * 1000
    # print('URL=%s' % resp.url)
    # print(resp.text)
    bindings = resp.json()['results']['bindings']
    new_total_elapsed = total_elapsed + elapsed
    page_num = (offset / page_size) + 1 if offset > 0 else 1
    print('Page %d, last request %dms, total elapsed %d seconds' % (page_num, elapsed, new_total_elapsed / 1000))
    is_continue = len(bindings) == page_size
    return (is_continue, new_total_elapsed)


def serial_mode():
    page_size = 50 * 1000
    total_elapsed = 0
    offset = 0
    while True:
        (is_continue, total_elapsed) = nextpage(total_elapsed, offset, page_size)
        if not is_continue:
            break
        offset += page_size


def parallel_mode():
    page_size = 50 * 1000
    from multiprocessing import Process
    start = now()
    p1 = Process(target=nextpage, args=(0, 0 * page_size, page_size))
    p1.start()
    p2 = Process(target=nextpage, args=(0, 1 * page_size, page_size))
    p2.start()
    p3 = Process(target=nextpage, args=(0, 2 * page_size, page_size))
    p3.start()
    p4 = Process(target=nextpage, args=(0, 3 * page_size, page_size))
    p4.start()
    p5 = Process(target=nextpage, args=(0, 4 * page_size, page_size))
    p5.start()
    p6 = Process(target=nextpage, args=(0, 5 * page_size, page_size))
    p6.start()
    p1.join()
    p2.join()
    p3.join()
    p4.join()
    p5.join()
    p6.join()
    user_et = -1
    wall_et = now() - start
    print('Parallel run took %d seconds wall time, %d seconds user time' % (wall_et, user_et / 1000))


print('Starting up...')
try:
    serial_mode()
    # parallel_mode()
except KeyboardInterrupt:
    pass
except Exception as e:
    print('Failed, error to follow')
    print(e)
