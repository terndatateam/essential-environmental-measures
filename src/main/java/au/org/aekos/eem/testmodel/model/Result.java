package au.org.aekos.eem.testmodel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
public class Result {
	
	// Results are constructed as key-value pairs to allow for different value types to be represented
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long resultId; //PK
	
	private String element;
	private String value;
	
	@ManyToOne
	private StorageSet storageSet;
	
	public Result() {}
	
	@Autowired
	public Result(String element, String value, StorageSet storageSet) {
		super();
		this.element = element;
		this.value = value;
		this.storageSet = storageSet;
	}
	
	
	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public long getValueId() {
		return resultId;
	}
	

}
