package au.org.aekos.eem.testmodel.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.Feature;
import au.org.aekos.eem.testmodel.model.Observation;

public interface ObservationRepository extends CrudRepository<Observation, Long>{
//	List<Observation> findAllOrderByObservationId();
	List<Observation> findAllByFeature(Feature feature);
}
