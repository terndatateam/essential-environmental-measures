package au.org.aekos.eem.testmodel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EemTestModelApplication {

	public static void main(String[] args) {
		String skipFlag = System.getenv("SKIP_POPULATE");
		if (skipFlag != null && skipFlag.length() > 0) {
			System.setProperty("--spring.jpa.hibernate.ddl-auto", "none");
		}
		SpringApplication.run(EemTestModelApplication.class, args);
	}
}
