package au.org.aekos.eem.testmodel.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
public class Time {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long timeId; //PK
	
//	@ManyToOne
//	private Observation observation;
	
	private String timeType;
	private Calendar dateTime;
	
	@ManyToOne
	private StorageSet storageSet;
	
	public Time() {}
	
	
	@Autowired
	public Time(String timeType, Calendar dateTime, StorageSet storageSet) {
		super();
		this.timeType = timeType;
		this.dateTime = dateTime;
		this.storageSet = storageSet;
	}



//	public Observation getObservation() {
//		return observation;
//	}
//
//	public void setObservation(Observation observation) {
//		this.observation = observation;
//	}

	public String getTimeType() {
		return timeType;
	}

	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	public Calendar getDateTime() {
		return dateTime;
	}

	public void setDateTime(Calendar dateTime) {
		this.dateTime = dateTime;
	}

	public StorageSet getStorageSet() {
		return storageSet;
	}

	public void setStorageSet(StorageSet storageSet) {
		this.storageSet = storageSet;
	}

	public long getTimeId() {
		return timeId;
	}
	
	
}
