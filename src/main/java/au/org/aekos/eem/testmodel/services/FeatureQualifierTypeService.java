package au.org.aekos.eem.testmodel.services;

import org.springframework.stereotype.Service;

import au.org.aekos.eem.testmodel.model.LutFeatureQualifierType;
import au.org.aekos.eem.testmodel.model.LutFeatureType;
import au.org.aekos.eem.testmodel.repositories.FeatureQualifierTypeRepository;

@Service
public class FeatureQualifierTypeService extends AbstractService{
	
	FeatureQualifierTypeRepository featureQualifierTypeRepository;

	public FeatureQualifierTypeService(FeatureQualifierTypeRepository featureQualifierTypeRepository) {
		super();
		this.featureQualifierTypeRepository = featureQualifierTypeRepository;
	}
	
//	public void addFeatureQualifierType(FeatureQualifierType featureQualifierType) {
//		featureQualifierTypeRepository.save(featureQualifierType);
//	}

	@Override
	public void add(Object object) {
		featureQualifierTypeRepository.save((LutFeatureQualifierType) object);
		
	}

	@Override
	public Object findByName(String name) {
		return featureQualifierTypeRepository.findByName(name);
	}
	
	@Override
	public boolean existsByName(String name) {
		LutFeatureQualifierType ft = featureQualifierTypeRepository.findByName(name);
		
		if(ft.getName().isEmpty())
			return false;
		return true;
	}

}
