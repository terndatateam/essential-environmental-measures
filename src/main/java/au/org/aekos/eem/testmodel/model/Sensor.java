package au.org.aekos.eem.testmodel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Sensor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long resultId; //PK
	
	@ManyToOne
	private Observation observation;
	
	private String element;
	private String value;
	
	@ManyToOne
	private StorageSet storageSet;

	public Sensor() {}
	
	public Sensor(Observation observation, String element, String value, StorageSet storageSet) {
		super();
		this.observation = observation;
		this.element = element;
		this.value = value;
		this.storageSet = storageSet;
	}
	

	public Observation getObservation() {
		return observation;
	}


	public void setObservation(Observation observation) {
		this.observation = observation;
	}


	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public StorageSet getStorageSet() {
		return storageSet;
	}

	public void setStorageSet(StorageSet storageSet) {
		this.storageSet = storageSet;
	}

	public long getResultId() {
		return resultId;
	}
	
	

}
