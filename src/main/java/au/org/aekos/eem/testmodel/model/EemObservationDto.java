package au.org.aekos.eem.testmodel.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class EemObservationDto {
	
	private long observationId;
	private String featureId;
	private String featureOfInterest;
	private String ultimateFeatureOfInterest;
	private String featureQualifier;
	private String originalFeatureOfInterest;
	private String protocol;
	private String protocolLink;
	private String property;
	private String propertyQualifier;
//	private Result result;
	private String value;
	private String rangeLow;
	private String rangeHigh;
	private String category;
	private String comment;
	private String standard;

	public EemObservationDto() {}
	
	
	public static EemObservationDto convertToDto(Observation obs) {
		EemObservationDto dto = new EemObservationDto();
		dto.setObservationId(obs.getObservationId());
		dto.setFeatureId(obs.getFeature().getFeatureId());
		dto.setFeatureOfInterest(obs.getFeature().getFeatureType().getName());
		if (obs.getUltimateFeature() != null) {
			dto.setUltimateFeatureOfInterest(obs.getUltimateFeature().getFeatureId());
		}
		dto.setFeatureQualifier(obs.getFeature().getFeatureQualifier().getName());
		dto.setOriginalFeatureOfInterest(obs.getFeature().getOriginalFeatureType().getName());
		dto.setProtocol(obs.getProcedure().getName());
		dto.setProtocolLink(obs.getProcedure().getProcedureId());
		dto.setProperty(obs.getProperty().getName());
		dto.setPropertyQualifier(obs.getPropertyQualifier().getName());
		
		// Construct result set
		// This is currently a bit dodgy as it assumes a quantitative measure
		Set<Result> results = obs.getValues();
		Iterator<Result> resIterator = results.iterator();
		HashMap<String,String> res = new HashMap<>();
		while(resIterator.hasNext()) {
			Result result = resIterator.next();
			res.put(result.getElement(), result.getValue());
		}
		dto.setValue(res.get("Value"));
		dto.setRangeLow(res.get("RangeLow"));
		dto.setRangeHigh(res.get("RangeHigh"));
		dto.setCategory(res.get("Category"));
		dto.setComment(res.get("Comment"));
		dto.setStandard(res.get("Standard"));
		
		
		
		return dto;
	}


	public long getObservationId() {
		return observationId;
	}


	public void setObservationId(long l) {
		this.observationId = l;
	}


	public String getFeatureId() {
		return featureId;
	}


	public void setFeatureId(String featureId) {
		this.featureId = featureId;
	}


	public String getFeatureOfInterest() {
		return featureOfInterest;
	}


	public void setFeatureOfInterest(String featureOfInterest) {
		this.featureOfInterest = featureOfInterest;
	}


	public String getUltimateFeatureOfInterest() {
		return ultimateFeatureOfInterest;
	}


	public void setUltimateFeatureOfInterest(String ultimateFeatureOfInterest) {
		if(!ultimateFeatureOfInterest.isEmpty())
			this.ultimateFeatureOfInterest = ultimateFeatureOfInterest;
	}


	public String getFeatureQualifier() {
		return featureQualifier;
	}


	public void setFeatureQualifier(String featureQualifier) {
		this.featureQualifier = featureQualifier;
	}


	public String getOriginalFeatureOfInterest() {
		return originalFeatureOfInterest;
	}


	public void setOriginalFeatureOfInterest(String originalFeatureOfInterest) {
		this.originalFeatureOfInterest = originalFeatureOfInterest;
	}


	public String getProtocol() {
		return protocol;
	}


	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}


	public String getProtocolLink() {
		return protocolLink;
	}


	public void setProtocolLink(String protocolLink) {
		this.protocolLink = protocolLink;
	}

	public String getProperty() {
		return property;
	}


	public void setProperty(String property) {
		this.property = property;
	}


	public String getPropertyQualifier() {
		return propertyQualifier;
	}


	public void setPropertyQualifier(String propertyQualifier) {
		this.propertyQualifier = propertyQualifier;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getRangeLow() {
		return rangeLow;
	}


	public void setRangeLow(String rangeLow) {
		this.rangeLow = rangeLow;
	}


	public String getRangeHigh() {
		return rangeHigh;
	}


	public void setRangeHigh(String rangeHigh) {
		this.rangeHigh = rangeHigh;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public String getStandard() {
		return standard;
	}


	public void setStandard(String standard) {
		this.standard = standard;
	}
	
	

}
