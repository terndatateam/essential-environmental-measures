package au.org.aekos.eem.testmodel.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.Feature;
import au.org.aekos.eem.testmodel.model.FeatureRelationship;

public interface FeatureRelationshipRepository extends CrudRepository<FeatureRelationship, Long>{
//	List<Procedure> findByNameIsNull();
//	List<Procedure> findByName(String name);
	List<FeatureRelationship> findAllByPrimaryFeature(Feature feature);
}