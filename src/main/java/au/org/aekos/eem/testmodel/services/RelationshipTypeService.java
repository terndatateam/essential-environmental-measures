package au.org.aekos.eem.testmodel.services;

import org.springframework.stereotype.Service;

import au.org.aekos.eem.testmodel.model.LutRelationshipType;
import au.org.aekos.eem.testmodel.repositories.RelationshipTypeRepository;

@Service
public class RelationshipTypeService extends AbstractService{
	
	private RelationshipTypeRepository relationshipTypeRepository;
	
	public RelationshipTypeService(RelationshipTypeRepository relationshipTypeRepository) {
		super();
		this.relationshipTypeRepository = relationshipTypeRepository;
	}

	@Override
	public void add(Object object) {
		relationshipTypeRepository.save((LutRelationshipType) object);
		
	}

	@Override
	public LutRelationshipType findByName(String name) {
		return relationshipTypeRepository.findByName(name);
	}
	
	@Override
	public boolean existsByName(String name) {
		LutRelationshipType ft = relationshipTypeRepository.findByName(name);
		
		if(ft == null)
			return false;
		
		if(ft.getName().isEmpty())
			return false;
		return true;
	}

}
