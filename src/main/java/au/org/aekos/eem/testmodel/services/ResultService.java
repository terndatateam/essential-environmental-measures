package au.org.aekos.eem.testmodel.services;

import org.springframework.stereotype.Service;

import au.org.aekos.eem.testmodel.model.Result;
import au.org.aekos.eem.testmodel.repositories.ResultRepository;

@Service
public class ResultService {

		ResultRepository resultRepository;

		public ResultService(ResultRepository resultRepository) {
			super();
			this.resultRepository = resultRepository;
		}
		
		public void add(Result result) {
			resultRepository.save(result);
		}
}
