package au.org.aekos.eem.testmodel.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.Feature;
import au.org.aekos.eem.testmodel.model.LutFeatureType;

public interface FeatureRepository extends CrudRepository<Feature, String>{
	List<Feature> findByFeatureTypeIsNull();
	List<Feature> findByFeatureType(LutFeatureType featureType);
}
