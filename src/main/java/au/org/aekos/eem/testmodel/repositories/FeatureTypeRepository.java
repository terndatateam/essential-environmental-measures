package au.org.aekos.eem.testmodel.repositories;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.LutFeatureType;

public interface FeatureTypeRepository extends CrudRepository<LutFeatureType, Long>{
	
	public LutFeatureType findByName(String name);
//	public boolean findByNameNotNull(String name);

}
