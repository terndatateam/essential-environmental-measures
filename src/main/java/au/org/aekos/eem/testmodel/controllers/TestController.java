package au.org.aekos.eem.testmodel.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import au.org.aekos.eem.testmodel.model.EemObservationDto;
import au.org.aekos.eem.testmodel.model.EemSampleDto;
import au.org.aekos.eem.testmodel.model.EemSampleDtoFactory;
import au.org.aekos.eem.testmodel.model.Feature;
import au.org.aekos.eem.testmodel.model.LutFeatureType;
import au.org.aekos.eem.testmodel.model.Observation;
import au.org.aekos.eem.testmodel.model.Procedure;
import au.org.aekos.eem.testmodel.repositories.FeatureRepository;
import au.org.aekos.eem.testmodel.repositories.FeatureTypeRepository;
import au.org.aekos.eem.testmodel.repositories.ProcedureRepository;
import au.org.aekos.eem.testmodel.services.FeatureService;
import au.org.aekos.eem.testmodel.services.ObservationService;

@RestController
@CrossOrigin(origins = "*")
public class TestController {
	
	private static final String SAMPLING_UNIT_FEATURE_TYPE_NAME = "SamplingUnit";

	@Autowired
	private ObservationService observationService;
	
	@Autowired
	private FeatureRepository featureRepository;
	
	@Autowired
	private FeatureService featureService;
	
	@Autowired
	private FeatureTypeRepository featureTypeRepository;
	
	@Autowired
	private EemSampleDtoFactory eemSampleDtoFactory;
	
	@Autowired
	private ProcedureRepository procedureRepo;
	
	@RequestMapping("/obs")
	public List<Observation> getObservations() {
		return observationService.getAllObservations();
	}

	
	@RequestMapping("/eem")
	public List<EemObservationDto> getEemObservations() {
		
		List<Observation> observationsIn = observationService.getAllObservations();
		List<EemObservationDto> observationsOut = new ArrayList<>();
		
		Iterator<Observation> it = observationsIn.iterator();
		while(it.hasNext()) {
			observationsOut.add(EemObservationDto.convertToDto(it.next()));
		}

		return observationsOut;
	}
	
	

	
	
	@RequestMapping("/eem/organism")
	public List<EemObservationDto> getEemOrganismObservations() {
		
		
		
		List<Observation> observationsIn = observationService.getAllObservations();
		return observationsIn.stream()
			.filter(e -> !SAMPLING_UNIT_FEATURE_TYPE_NAME.equals(e.getFeature().getFeatureType().getName())) // note the !
			.map(e -> EemObservationDto.convertToDto(e))
			.collect(Collectors.toList());
	}
	
	@RequestMapping("/eem/samplingunit")
	public List<EemSampleDto> getEemSuObservations() {
		List<Observation> observationsIn = observationService.getAllObservations();
		
		
//		LutFeatureType featureType = featureTypeRepository.findByName("SampledArea");
		
		String sampledArea = "SampledArea";
		
		System.out.println("FHFHFHF");
		
		List<Feature> featuresIn = featureService.getAllFeatures();
		
		System.out.println("DDD: " + featuresIn.size());
		
//		return featuresIn.stream()
//			.filter(e -> SAMPLING_UNIT_FEATURE_TYPE_NAME.equals(e.getFeatureType().getName()))
////			.map(e -> EemObservationDto.convertToDto(e))
//			.map(e -> EemSampleDto.convertToDto(e))
//			.collect(Collectors.toList());
//		
		
		
		List<EemSampleDto> featuresOut = new ArrayList<>();
		
		Iterator<Feature> it = featuresIn.iterator();
		while(it.hasNext()) {
			featuresOut.add(eemSampleDtoFactory.convertToDto(it.next()));
		}
		
		return featuresOut;
	}
	
	
	

	
	

	// call this with http://localhost:8080/procByName
	@RequestMapping("/procByName")
	@Transactional // we need this because Procedure uses @Lob, see https://stackoverflow.com/q/3164072/1410035
	public List<Procedure> getProceduresByName() {
		// Demonstrates how you can search for records by some property.
		// It's a bit involved because you have to follow section 1.2 from https://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html
		// ...where you add a method to the Repository interface like .findByBlah(String blah)
		// ...and the magic behind the scenes knows you want to search by the "Blah" property.
		// Then you use that repository to search by that property anywhere. You should
		// probably create a method in the ProcedureService and call that from here
		// but who cares for this prototype. The Services are just wasting time.
		return procedureRepo.findByName("Vegetation Assessment Method");
	}
}
