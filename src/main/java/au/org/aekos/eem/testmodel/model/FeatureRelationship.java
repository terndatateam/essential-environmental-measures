package au.org.aekos.eem.testmodel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class FeatureRelationship {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long propertyTypeId; //PK
	
	@ManyToOne
	private Feature primaryFeature;
	@ManyToOne
	private LutRelationshipType relationship;
	@ManyToOne
	private Feature relatedFeature;
	
	public FeatureRelationship() {}

	public FeatureRelationship(Feature primaryFeature, LutRelationshipType relationship, Feature relatedFeature) {
		super();
		this.primaryFeature = primaryFeature;
		this.relationship = relationship;
		this.relatedFeature = relatedFeature;
	}

	public Feature getPrimaryFeature() {
		return primaryFeature;
	}

	public void setPrimaryFeature(Feature primaryFeature) {
		this.primaryFeature = primaryFeature;
	}

	public LutRelationshipType getRelationship() {
		return relationship;
	}

	public void setRelationship(LutRelationshipType relationship) {
		this.relationship = relationship;
	}

	public Feature getRelatedFeature() {
		return relatedFeature;
	}

	public void setRelatedFeature(Feature relatedFeature) {
		this.relatedFeature = relatedFeature;
	}

	public long getPropertyTypeId() {
		return propertyTypeId;
	}


	
}
