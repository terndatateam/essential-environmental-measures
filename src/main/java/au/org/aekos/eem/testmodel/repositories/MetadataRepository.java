package au.org.aekos.eem.testmodel.repositories;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.Metadata;

public interface MetadataRepository extends CrudRepository<Metadata, String>{
	

}
