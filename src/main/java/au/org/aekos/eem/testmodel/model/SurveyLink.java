package au.org.aekos.eem.testmodel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class SurveyLink {
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long Id; //PK
	



	@OneToOne
	private Feature studyLocation;
	
	private String studyLocationSubgraphId;
	
	@ManyToOne
	private Metadata metadata;
	

	public SurveyLink() {}


	public SurveyLink(Feature studyLocation, String studyLocationSubgraphId, Metadata metadata) {
		super();
		this.studyLocation = studyLocation;
		this.studyLocationSubgraphId = studyLocationSubgraphId;
		this.metadata = metadata;
	}


	public long getId() {
		return Id;
	}


	public void setId(long id) {
		Id = id;
	}
	
	public Feature getStudyLocation() {
		return studyLocation;
	}


	public void setStudyLocation(Feature studyLocation) {
		this.studyLocation = studyLocation;
	}


	public String getStudyLocationSubgraphId() {
		return studyLocationSubgraphId;
	}


	public void setStudyLocationSubgraphId(String studyLocationSubgraphId) {
		this.studyLocationSubgraphId = studyLocationSubgraphId;
	}


	public Metadata getMetadata() {
		return metadata;
	}


	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}




}
