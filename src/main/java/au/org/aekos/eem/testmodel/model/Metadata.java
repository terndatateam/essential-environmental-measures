package au.org.aekos.eem.testmodel.model;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Metadata {
	
	@Id
	String surveyId;
	
	String persistentsurveyId;
	String surveyName;
	String organisation;
	String custodian;
	
	String rights;
	String licence;
	String citation;
	
	String dateModified;
	String dateAccessioned;
	String language;
	
	String surveyType;
	String surveyMethod;
	String methodLink;
	
	
	
	

	public Metadata() {}
	
	public Metadata(String surveyId) {
		super();
		this.surveyId = surveyId;
	}

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	public String getPersistentsurveyId() {
		return persistentsurveyId;
	}

	public void setPersistentsurveyId(String persistentsurveyId) {
		this.persistentsurveyId = persistentsurveyId;
	}

	public String getSurveyName() {
		return surveyName;
	}

	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getCustodian() {
		return custodian;
	}

	public void setCustodian(String custodian) {
		this.custodian = custodian;
	}

	public String getRights() {
		return rights;
	}

	public void setRights(String rights) {
//		this.rights = rights;
		if(rights.length() > 254) {
			this.rights = rights.substring(0, 254);
		}else {
			this.rights = rights;
		}
		System.out.println("RightsLength: " + this.rights.length());
	}

	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}

	public String getCitation() {
		return citation;
	}

	public void setCitation(String citation) {
		if(citation.length() > 254) {
			this.citation = citation.substring(0, 254);
		}else {
			this.citation = citation;
		}
		System.out.println("CitLength: " + this.citation.length());
	}


	public String getDateModified() {
		return dateModified;
	}

	public void setDateModified(String dateModified) {
		this.dateModified = dateModified;
	}

	public String getDateAccessioned() {
		return dateAccessioned;
	}

	public void setDateAccessioned(String dateAccessioned) {
		this.dateAccessioned = dateAccessioned;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSurveyType() {
		return surveyType;
	}

	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}

	public String getSurveyMethod() {
		return surveyMethod;
	}

	public void setSurveyMethod(String surveyMethod) {
		this.surveyMethod = surveyMethod;
	}

	public String getMethodLink() {
		return methodLink;
	}

	public void setMethodLink(String methodLink) {
		this.methodLink = methodLink;
	}

	@Override
	public String toString() {
		return "Metadata [surveyId=" + surveyId + ", persistentsurveyId=" + persistentsurveyId + ", surveyName="
				+ surveyName + ", organisation=" + organisation + ", custodian=" + custodian + ", rights=" + rights
				+ ", licence=" + licence + ", citation=" + citation + ", dateModified=" + dateModified
				+ ", dateAccessioned=" + dateAccessioned + ", language=" + language + ", surveyType=" + surveyType
				+ ", surveyMethod=" + surveyMethod + ", methodLink=" + methodLink + "]";
	}
	
	
}
