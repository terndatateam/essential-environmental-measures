package au.org.aekos.eem.testmodel.repositories;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.LutPropertyType;

public interface PropertyTypeRepository extends CrudRepository<LutPropertyType, Long>{
	public LutPropertyType findByName(String name);
}
