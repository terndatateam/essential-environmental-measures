package au.org.aekos.eem.testmodel.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.org.aekos.eem.testmodel.repositories.FeatureRelationshipRepository;
import au.org.aekos.eem.testmodel.repositories.ObservationRepository;
import au.org.aekos.eem.testmodel.repositories.PropertyTypeRepository;
import au.org.aekos.eem.testmodel.services.ObservationService;

public class EemSampleDto {
	
//	private long observationId;

	
	// Metadata
	private String custodian;
	private String rights;
	private String citation;
	private String dateModified;
	private String language;
	
	// Survey
	private String surveyId;
	private String surveyName;
	private String surveyOrganisation;
	private String surveyType;
	private String surveyMethodology;
	private String methodologyDescription;
	
	// Study Location
	private String locationId;
	private String originalLocationId;
	private String province;
	private String datum;
	private String longitude;
	private String latitude;
	private String locationDescription;
	private String aspect;
	private String slope;
	private String landformPattern;
	private String landformElement;
	private String elevation;
	
	// Visit
	private String visitId;
	private String visitDate;
	private String visitOrganisation;
	private String visitObservers;
	private String siteDescription;
	
	// Additional attributes
	private String condition;
	private String structuralForm;
	private String ownersClassification;
	private String currentClassification;
	
	// Sampling Unit
	private String samplingUnitId;
	private String samplingUnitArea;
	private String samplingUnitShape;
	
	// Other
	private String dummyText;
	

	public EemSampleDto() {}
	
	private String getDummyText() {
		if(dummyText != null)
		return dummyText;
		
		return "Dummy";
	}

	public void setDummyText(String dummyText) {
		this.dummyText = dummyText;
	}

	
	public String getCustodian() {
		if(custodian != null)
			return custodian;
		
		return getDummyText();
	}



	public void setCustodian(String custodian) {
		this.custodian = custodian;
	}



	public String getRights() {
		
		if(rights != null)
			return rights;
		
		return getDummyText();
	}



	public void setRights(String rights) {
		this.rights = rights;
	}



	public String getCitation() {
		if(citation != null)
			return citation;
		
		return getDummyText();
	}



	public void setCitation(String citation) {
		this.citation = citation;
	}



	public String getDateModified() {
		if(dateModified != null)
			return dateModified;
		
		return getDummyText();
	}



	public void setDateModified(String dateModified) {
		this.dateModified = dateModified;
	}



	public String getLanguage() {
		if(language != null)
			return language;
		
		return getDummyText();
	}



	public void setLanguage(String language) {
		this.language = language;
	}



	public String getSurveyId() {
		if(surveyId != null)
			return surveyId;
		
		return getDummyText();
	}



	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}



	public String getSurveyName() {
		if(surveyName != null)
			return surveyName;
		
		return getDummyText();
	}



	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}



	public String getSurveyOrganisation() {
		if(surveyOrganisation != null)
			return surveyOrganisation;
		
		return getDummyText();
	}



	public void setSurveyOrganisation(String surveyOrganisation) {
		this.surveyOrganisation = surveyOrganisation;
	}



	public String getSurveyType() {
		if(surveyType != null)
			return surveyType;
		
		return getDummyText();
	}



	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}



	public String getSurveyMethodology() {
		if(surveyMethodology != null)
			return surveyMethodology;
		
		return getDummyText();
	}



	public void setSurveyMethodology(String surveyMethodology) {
		this.surveyMethodology = surveyMethodology;
	}



	public String getMethodologyDescription() {
		if(methodologyDescription != null)
			return methodologyDescription;
		
		return getDummyText();
	}



	public void setMethodologyDescription(String methodologyDescription) {
		this.methodologyDescription = methodologyDescription;
	}



	public String getLocationId() {
		if(locationId != null)
			return locationId;
		
		return getDummyText();
	}



	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}



	public String getOriginalLocationId() {
		if(originalLocationId != null)
			return originalLocationId;
		
		return getDummyText();
	}



	public void setOriginalLocationId(String originalLocationId) {
		this.originalLocationId = originalLocationId;
	}



	public String getProvince() {
		if(province != null)
			return province;
		
		return getDummyText();
	}



	public void setProvince(String province) {
		this.province = province;
	}



	public String getDatum() {
		if(datum != null)
			return datum;
		
		return getDummyText();
	}



	public void setDatum(String datum) {
		this.datum = datum;
	}



	public String getLongitude() {
		if(longitude != null)
			return longitude;
		
		return getDummyText();
	}



	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}



	public String getLatitude() {
		if(latitude != null)
			return latitude;
		
		return getDummyText();
	}



	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}



	public String getLocationDescription() {
		if(locationDescription != null)
			return locationDescription;
		
		return getDummyText();
	}



	public void setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
	}



	public String getAspect() {
		if(aspect != null)
			return aspect;
		
		return getDummyText();
	}



	public void setAspect(String aspect) {
		this.aspect = aspect;
	}



	public String getSlope() {
		if(slope != null)
			return slope;
		
		return getDummyText();
	}



	public void setSlope(String slope) {
		this.slope = slope;
	}



	public String getLandformPattern() {
		if(landformPattern != null)
			return landformPattern;
		
		return getDummyText();
	}



	public void setLandformPattern(String landformPattern) {
		this.landformPattern = landformPattern;
	}



	public String getLandformElement() {
		if(landformElement != null)
			return landformElement;
		
		return getDummyText();
	}



	public void setLandformElement(String landformElement) {
		this.landformElement = landformElement;
	}



	public String getElevation() {
		if(elevation != null)
			return elevation;
		
		return getDummyText();
	}



	public void setElevation(String elevation) {
		this.elevation = elevation;
	}



	public String getVisitId() {
		if(visitId != null)
			return visitId;
		
		return getDummyText();
	}



	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}



	public String getVisitDate() {
		if(visitDate != null)
			return visitDate;
		
		return getDummyText();
	}



	public void setVisitDate(String visitDate) {
		this.visitDate = visitDate;
	}



	public String getVisitOrganisation() {
		if(visitOrganisation != null)
			return visitOrganisation;
		
		return getDummyText();
	}



	public void setVisitOrganisation(String visitOrganisation) {
		this.visitOrganisation = visitOrganisation;
	}



	public String getVisitObservers() {
		if(visitObservers != null)
			return visitObservers;
		
		return getDummyText();
	}



	public void setVisitObservers(String visitObservers) {
		this.visitObservers = visitObservers;
	}



	public String getSiteDescription() {
		if(siteDescription != null)
			return siteDescription;
		
		return getDummyText();
	}



	public void setSiteDescription(String siteDescription) {
		this.siteDescription = siteDescription;
	}



	public String getCondition() {
		if(condition != null)
			return condition;
		
		return getDummyText();
	}



	public void setCondition(String condition) {
		this.condition = condition;
	}



	public String getStructuralForm() {
		if(structuralForm != null)
			return structuralForm;
		
		return getDummyText();
	}



	public void setStructuralForm(String structuralForm) {
		this.structuralForm = structuralForm;
	}



	public String getOwnersClassification() {
		if(ownersClassification != null)
			return ownersClassification;
		
		return getDummyText();
	}



	public void setOwnersClassification(String ownersClassification) {
		this.ownersClassification = ownersClassification;
	}



	public String getCurrentClassification() {
		if(currentClassification != null)
			return currentClassification;
		
		return getDummyText();
	}



	public void setCurrentClassification(String currentClassification) {
		this.currentClassification = currentClassification;
	}



	public String getSamplingUnitId() {
		if(samplingUnitId != null)
			return samplingUnitId;
		
		return getDummyText();
	}



	public void setSamplingUnitId(String samplingUnitId) {
		this.samplingUnitId = samplingUnitId;
	}



	public String getSamplingUnitArea() {
		if(samplingUnitArea != null)
			return samplingUnitArea;
		
		return getDummyText();
	}



	public void setSamplingUnitArea(String samplingUnitArea) {
		this.samplingUnitArea = samplingUnitArea;
	}



	public String getSamplingUnitShape() {
		if(samplingUnitShape != null)
			return samplingUnitShape;
		
		return getDummyText();
	}



	public void setSamplingUnitShape(String samplingUnitShape) {
		this.samplingUnitShape = samplingUnitShape;
	}
	
	
}
