package au.org.aekos.eem.testmodel.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.Procedure;

public interface ProcedureRepository extends CrudRepository<Procedure, Long>{
	List<Procedure> findByNameIsNull();
	List<Procedure> findByName(String name);
}
