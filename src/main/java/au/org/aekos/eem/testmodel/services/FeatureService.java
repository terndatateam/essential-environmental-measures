package au.org.aekos.eem.testmodel.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import au.org.aekos.eem.testmodel.model.Feature;
import au.org.aekos.eem.testmodel.model.LutRelationshipType;
import au.org.aekos.eem.testmodel.model.Observation;
import au.org.aekos.eem.testmodel.repositories.FeatureRepository;

@Service
public class FeatureService {
	
	private FeatureRepository featureRepository;

	public FeatureService(FeatureRepository featureRepository) {
		super();
		this.featureRepository = featureRepository;
	}
	
	public void add(Feature feature) {
		featureRepository.save(feature);
		
	}
	
	public Optional<Feature> findById(String featureId) {
		return featureRepository.findById(featureId);
	}
	
	public List<Feature> getAllFeatures() {
		List<Feature> featList = new ArrayList<>();
		featureRepository.findAll().forEach(featList::add);
		return featList;
	}

}
