package au.org.aekos.eem.testmodel.sparql;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.springframework.web.util.HtmlUtils;

public class QueryManager {
	
	private static String endPoint;
	private static String resourceContainer;
	private String query;
	private boolean respectLimit = true;

	public QueryManager(String endpoint, String resourceContainer) {
		QueryManager.endPoint = endpoint;
		QueryManager.resourceContainer = resourceContainer;
	}

//	public void reset() {
//
//		System.out.println("Query Reset");
//	}
	
	public void addLimit(int limit) {
//		String queryOut = query + "/nlimit " + limit;
        if(!respectLimit) {
        	return;
        }
        
		StringBuilder sb = new StringBuilder();
		sb.append(query);
		sb.append("\n");
		sb.append("limit " + limit);
		query = sb.toString();
//		System.out.println("QUERYLIMITSET: " + query);
	}
	
	
	public void loadQuery(String fileName){
		InputStream sparqlIS = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceContainer + fileName);
		OutputStream out = new ByteArrayOutputStream();
		try {
			copy(sparqlIS, out);
			query = out.toString();
		} catch (IOException e) {
			throw new RuntimeException("Failed to read file " + fileName, e);
		}
	}
	
	
	public List<String[]> performQuery() throws Throwable {
		String urlEncodedQuery = URLEncoder.encode(query, "UTF-8");
		try {
			final boolean nobodyLikesYouHeader = true;
			List<String[]> result = postForTsv("query=" + urlEncodedQuery, nobodyLikesYouHeader);
//			for (String[] curr : csv) {
//				System.out.println(Arrays.toString(curr));
//			}
			return result;
		} catch (Exception e) {
			throw new RuntimeException("Failed to perform query", e);
		}
	}
	
	protected List<String> performQuerySimple(String query) throws Throwable {
		List<String[]> complex = performQuery();
		
		List<String> result = new LinkedList<>();
		Iterator<String[]> iterator = complex.iterator();
		while(iterator.hasNext()){
			result.add(iterator.next()[0]);
		}
		
		return result;
	}
	
	private static final int BUFFER_SIZE = 4096;
	private static int copy(InputStream in, OutputStream out) throws IOException {
		int byteCount = 0;
		byte[] buffer = new byte[BUFFER_SIZE];
		int bytesRead = -1;
		while ((bytesRead = in.read(buffer)) != -1) {
			out.write(buffer, 0, bytesRead);
			byteCount += bytesRead;
		}
		out.flush();
		return byteCount;
	}
	
	private List<String[]> postForTsv(String formData, boolean isDiscardHeader) throws Exception {
		final String acceptMime = "text/tab-separated-values";
		List<String[]> result = new LinkedList<>();
		URL obj = new URL(endPoint);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		//add request header
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept", acceptMime);
		con.setRequestProperty("Accept-Language", "en-AU,en-GB;q=0.8,en-US;q=0.6,en;q=0.4");
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(formData);
		wr.flush();
		wr.close();
		int responseCode = con.getResponseCode();
		boolean isSuccess = responseCode == 200;
		if (!isSuccess) {
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
			String inputLine;
			StringBuilder errorBody = new StringBuilder();
			while ((inputLine = in.readLine()) != null) {
				errorBody.append(HtmlUtils.htmlUnescape(inputLine));
				errorBody.append("\n");
			}
			throw new RuntimeException(String.format("Failed to perform query. HTTP status code = %s, errorBody = %s", responseCode, errorBody.toString()));
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		boolean isFirst = true;
		while ((inputLine = in.readLine()) != null) {
			if (isDiscardHeader && isFirst) {
				isFirst = false;
				continue;
			}
			result.add(inputLine.split("\t"));
		}
		in.close();
		return result;
	}

	public void replaceContent(String target, String replacement) {
		String query1 = query.replace(target, replacement);
		query = query1;
	}

	public boolean isRespectLimit() {
		return respectLimit;
	}

	public void setRespectLimit(boolean respectLimit) {
		this.respectLimit = respectLimit;
	}
}
