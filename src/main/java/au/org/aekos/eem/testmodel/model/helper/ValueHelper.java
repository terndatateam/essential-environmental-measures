package au.org.aekos.eem.testmodel.model.helper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.springframework.stereotype.Component;

import au.org.aekos.eem.testmodel.model.Result;

@Component
public class ValueHelper {
	
	private Set<Result> resultSet;
	private HashMap<String, String> resultMap;
	private String type;

	public ValueHelper() {}
	
	
	
	public void loadResultSet (Set<Result> resultSet) {
		this.resultSet = resultSet;
		resultMap = new HashMap<String, String>();
		
		Iterator<Result> it = resultSet.iterator();
		while(it.hasNext()) {
			Result result = it.next();
			resultMap.put(result.getElement(), result.getValue());
		}
		
		if(resultMap.containsKey("Type")) {
			type = resultMap.get("Type");
		}else {
			type = null;
		}
		
	}
	
	public String makeString() {
		StringBuilder sb = new StringBuilder();
		
		if(type == null || type == "")
			return "";
		
		if(type == "QuantitativeMeasure") {
			
		}
		
		
		return sb.toString();
	}

}
