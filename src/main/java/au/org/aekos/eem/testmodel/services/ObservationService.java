package au.org.aekos.eem.testmodel.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.org.aekos.eem.testmodel.model.Observation;
import au.org.aekos.eem.testmodel.repositories.ObservationRepository;

@Service
public class ObservationService {

	ObservationRepository observationRepository;

	@Autowired
	public ObservationService(ObservationRepository observationRepository) {
		super();
		this.observationRepository = observationRepository;
	}
	
	public List<Observation> getAllObservations() {
		List<Observation> obsList = new ArrayList<>();
		observationRepository.findAll().forEach(obsList::add);
//		observationRepository.findAllOrderByObservationId().forEach(obsList::add);
		return obsList;
	}
	
	public void add(Observation observation) {
		observationRepository.save(observation);
	}
	
}
