package au.org.aekos.eem.testmodel.repositories;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.LutRelationshipType;

public interface RelationshipTypeRepository  extends CrudRepository<LutRelationshipType, Long>{
	public LutRelationshipType findByName(String name);
}


