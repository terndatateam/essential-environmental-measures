package au.org.aekos.eem.testmodel.repositories;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.StorageSet;

public interface StorageSetRepository extends CrudRepository<StorageSet, Long>{

}
