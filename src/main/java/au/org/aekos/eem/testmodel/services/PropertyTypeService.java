package au.org.aekos.eem.testmodel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.org.aekos.eem.testmodel.model.LutPropertyType;
import au.org.aekos.eem.testmodel.repositories.PropertyTypeRepository;

@Service
public class PropertyTypeService extends AbstractService{
	
	private PropertyTypeRepository propertyTypeRepository;
	
	@Autowired
	public PropertyTypeService(PropertyTypeRepository propertyTypeRepository) {
		super();
		this.propertyTypeRepository = propertyTypeRepository;
	}

	@Override
	public void add(Object object) {
		propertyTypeRepository.save((LutPropertyType) object);
	}

	@Override
	public LutPropertyType findByName(String name) {
		return propertyTypeRepository.findByName(name);
	}

	@Override
	public boolean existsByName(String name) {
		LutPropertyType ft = propertyTypeRepository.findByName(name);
		
		if(ft.getName().isEmpty())
			return false;
		return true;
	}
}
