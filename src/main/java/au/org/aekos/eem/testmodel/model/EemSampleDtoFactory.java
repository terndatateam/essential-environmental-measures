package au.org.aekos.eem.testmodel.model;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.org.aekos.eem.testmodel.repositories.FeatureRelationshipRepository;
import au.org.aekos.eem.testmodel.repositories.ObservationRepository;
import au.org.aekos.eem.testmodel.repositories.PropertyTypeRepository;
import au.org.aekos.eem.testmodel.services.ObservationService;

@Component
public class EemSampleDtoFactory {
	
	@Autowired
	private ObservationService observationService;
	@Autowired
	private ObservationRepository observationRepository;
	@Autowired
	private PropertyTypeRepository propertyTypeRepository;
	@Autowired
	private FeatureRelationshipRepository featureRelationshipRepository;
	
	public EemSampleDtoFactory() {
		// TODO Auto-generated constructor stub
	}

	public EemSampleDto convertToDto(Feature feature) {
		EemSampleDto dto = new EemSampleDto();
		
		System.out.println("Starting Sample DTO");
		
		System.out.println("SADASD: " + feature.getFeatureId());
		
		List<FeatureRelationship> relationships = featureRelationshipRepository.findAllByPrimaryFeature(feature);
		Iterator<FeatureRelationship> aaa = relationships.iterator();
		

		while(aaa.hasNext()) {
			FeatureRelationship fr = aaa.next();
			Feature relatedFeature = fr.getRelatedFeature();
			if(relatedFeature.getFeatureType().getName() == "StudyLocation") {
				System.out.println("FJFJFJFJFJJFJFFJ");
			}
		}
		
		
		
		
		
		
		
		
//		LutPropertyType areaProperty = propertyTypeRepository.findByName("Area");
//		
//
//		// Get observations and put in a map
//		List<Observation> observations = observationRepository.findAllByFeature(feature);
//		Iterator<Observation> it = observations.iterator();
//		HashMap<LutPropertyType,Observation> obsMap = new HashMap<LutPropertyType, Observation>();
//		while(it.hasNext()) {
//			Observation obs = it.next();
//			LutPropertyType property = obs.getProperty();
//			obsMap.put(property, obs);
//		}
//		
//		if(obsMap.containsKey(areaProperty)) {
//			dto.samplingUnitArea = "200m";
//			Observation area = obsMap.get(areaProperty);
//		}
		
//		if(obsMap.containsKey())
		
		return dto;
	}
	
}
