package au.org.aekos.eem.testmodel.repositories;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.Sensor;

public interface SensorRepository extends CrudRepository<Sensor, Long>{

}
