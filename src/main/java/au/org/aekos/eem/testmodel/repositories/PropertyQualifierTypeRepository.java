package au.org.aekos.eem.testmodel.repositories;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.LutPropertyQualifierType;

public interface PropertyQualifierTypeRepository extends CrudRepository<LutPropertyQualifierType, Long>{
	public LutPropertyQualifierType findByName(String name);
}
