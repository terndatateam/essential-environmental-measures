package au.org.aekos.eem.testmodel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.org.aekos.eem.testmodel.model.Procedure;
import au.org.aekos.eem.testmodel.repositories.ProcedureRepository;

@Service
public class ProcedureService {

	private ProcedureRepository procedureRepository;

	@Autowired
	public ProcedureService(ProcedureRepository procedureRepository) {
		super();
		this.procedureRepository = procedureRepository;
	}
	
	public void add(Procedure procedure) {
		procedureRepository.save(procedure);
	}
}
