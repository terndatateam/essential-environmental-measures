package au.org.aekos.eem.testmodel.model.helper;

import java.lang.reflect.Constructor;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.org.aekos.eem.testmodel.services.AbstractService;

@Component
public class TypeVocabHelper {
	
	@Autowired
	public TypeVocabHelper() {}
	
	
	
	public <T> Object retreiveFromLut(HashMap<String, Object> map, AbstractService service, Class<T> objectClass, String typeValue) {
		// Check if exists in hashMap
		if(!map.containsKey(typeValue)) {
			// Need to check DB
			Object typeObject = service.findByName(typeValue);
			if(typeObject == null) {
				// Doesn't exist so create
				typeObject = makeClassInstance(objectClass, typeValue);
				service.add(typeObject);
			}
			// Add to the hashMap
			map.put(typeValue, typeObject);
		}
		return map.get(typeValue);
	}
	
	
	private <T> T makeClassInstance(Class<T> objectClass, String constructorParam) {
		Constructor<T> constructor;
		T newObject = null;
		try {
			constructor = objectClass.getDeclaredConstructor(new Class[] {String.class});
			newObject = (T) constructor.newInstance(new Object[] {constructorParam});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newObject;
	}

}
