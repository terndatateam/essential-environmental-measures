package au.org.aekos.eem.testmodel.services;

import org.springframework.stereotype.Service;

import au.org.aekos.eem.testmodel.model.LutFeatureType;
import au.org.aekos.eem.testmodel.repositories.FeatureTypeRepository;

@Service
public class FeatureTypeService extends AbstractService{

	FeatureTypeRepository featureTypeRepository;

	public FeatureTypeService(FeatureTypeRepository featureTypeRepository) {
		super();
		this.featureTypeRepository = featureTypeRepository;
	}

//	public void add(FeatureType featureType) {
//		featureTypeRepository.save(featureType);
//	}

	@Override
	public LutFeatureType findByName(String name) {
		return featureTypeRepository.findByName(name);
	}

	@Override
	public void add(Object object) {
		featureTypeRepository.save((LutFeatureType) object);
	}
	
	@Override
	public boolean existsByName(String name) {
		LutFeatureType ft = featureTypeRepository.findByName(name);
		
		if(ft.getName().isEmpty())
			return false;
		return true;
	}
}
