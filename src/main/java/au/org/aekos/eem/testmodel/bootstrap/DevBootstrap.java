package au.org.aekos.eem.testmodel.bootstrap;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import au.org.aekos.eem.testmodel.model.Feature;
import au.org.aekos.eem.testmodel.model.LutPropertyQualifierType;
import au.org.aekos.eem.testmodel.model.LutPropertyType;
import au.org.aekos.eem.testmodel.model.LutRelationshipType;
import au.org.aekos.eem.testmodel.model.Metadata;
import au.org.aekos.eem.testmodel.model.Observation;
import au.org.aekos.eem.testmodel.model.Procedure;
import au.org.aekos.eem.testmodel.model.Result;
import au.org.aekos.eem.testmodel.model.StorageSet;
import au.org.aekos.eem.testmodel.model.SurveyLink;
import au.org.aekos.eem.testmodel.model.TempMethod;
import au.org.aekos.eem.testmodel.model.Time;
import au.org.aekos.eem.testmodel.model.helper.FeatureHelper;
import au.org.aekos.eem.testmodel.model.helper.TypeVocabHelper;
import au.org.aekos.eem.testmodel.repositories.FeatureRepository;
import au.org.aekos.eem.testmodel.repositories.MetadataRepository;
import au.org.aekos.eem.testmodel.repositories.ProcedureRepository;
import au.org.aekos.eem.testmodel.repositories.StorageSetRepository;
import au.org.aekos.eem.testmodel.repositories.SurveyLinkRepository;
import au.org.aekos.eem.testmodel.repositories.TempMethodRepository;
import au.org.aekos.eem.testmodel.repositories.TimeRepository;
import au.org.aekos.eem.testmodel.services.ObservationService;
import au.org.aekos.eem.testmodel.services.ProcedureService;
import au.org.aekos.eem.testmodel.services.PropertyQualifierTypeService;
import au.org.aekos.eem.testmodel.services.PropertyTypeService;
import au.org.aekos.eem.testmodel.services.RelationshipTypeService;
import au.org.aekos.eem.testmodel.services.ResultService;
import au.org.aekos.eem.testmodel.sparql.QueryManager;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent>{
	
	private static final Logger logger = LoggerFactory.getLogger(DevBootstrap.class);
	// CONFIG STUFF
//	private static final String endpoint = "http://localhost:8083/fuseki/aekos2/";
	//private static final String endpoint = "http://129.127.180.97:8083/fuseki/aekos2/";
	private static final String endpoint = "http://129.127.180.163:3030/ds/";
//	private static final String resourceContainer = "au/org/aekos/eem/sparql/";
	private static final String resourceContainer = "sparql/";
	
	private QueryManager queryManager = new QueryManager(endpoint,resourceContainer);
	
	private HashMap<String, Object> propertyTypeMap = new HashMap<>();
	private HashMap<String, Object> propertyQualifierTypeMap = new HashMap<>();
	
	
	// STUFF TO INJECT
//	@Autowired
//	private OriginalFeatureTypeRepository originalFeatureTypeRepository;
	@Autowired
	private StorageSetRepository storageSetRepository;
	@Autowired
	private TimeRepository timeRepository;
	@Autowired
	private TempMethodRepository tempMethodRepository;
	@Autowired
	private ProcedureRepository procedureRepository;
	@Autowired
	private FeatureRepository featureRepository;
	@Autowired
	private MetadataRepository metadataRepository;
	@Autowired
	private SurveyLinkRepository surveyLinkRepository;
	
	@Autowired
	private FeatureHelper featureHelper;
	@Autowired
	private TypeVocabHelper typeVocabHelper;
	
	@Autowired
	private PropertyTypeService propertyTypeService;
	@Autowired
	private PropertyQualifierTypeService propertyQualifierTypeService;
	@Autowired
	private ObservationService observationService;
	@Autowired
	private ProcedureService procedureService;
	@Autowired
	private ResultService resultService;
	@Autowired
	private RelationshipTypeService relationshipTypeService;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		String skipFlag = System.getenv("SKIP_POPULATE");
		if (skipFlag != null && skipFlag.length() > 0) {
			logger.info("Skipping DB population");
			return;
		}
		runSparql();
		
	}
	
	public void runSparql() {
		final String[] targetDatasets = new String[] {
			//"<http://www.aekos.org.au/ontology/1.0.0/wadec_ravensthorpe#>",
			//"<http://www.aekos.org.au/ontology/1.0.0/dewnr_bdbsa#>",
			//"<http://www.aekos.org.au/ontology/1.0.0/qld_corveg#>",
//			"<http://www.aekos.org.au/ontology/1.0.0/tern_ausplots#>",
            "<http://www.aekos.org.au/ontology/1.0.0/oeh_vis#>",
		};
		
		queryManager.setRespectLimit(false); // Set to false to ignore all testing limits
		
		for (String curr: targetDatasets) {
			logger.info("Currently processing dataset: " + curr);
			StorageSet storageSet = new StorageSet(curr);
			
			// Save the storageSet
			storageSetRepository.save(storageSet);
			
			// Process all the survey stuff
			populateSurveyDetails("getSurveyDetails.rq", curr, storageSet);
			
			// Process the studyLocations
			populateStudyLocations("getStudyLocations.rq", curr, storageSet);
			
			// Process the survey links
			populateSurveyLinks("mapSurveyToStudyLocation.rq", curr, storageSet);
			
			// Process Sampling Units
			populateSamplingFeatures("getSamplingUnitObservations.rq", curr, storageSet);
			
			// Process all the OrganismGroups
			processBaseOrganismObservations("getOrganismGroupBaseData.rq", curr, storageSet);	
			
			// Get list of Observations that have height data
			 processHeightObservations("getHeightObservations2.rq", curr, storageSet);
			
			// Get taxonomic observations
			populateTaxonomicObservations("getTaxonObservations.rq", curr, storageSet);
		}
	}
	
	private void populateSamplingFeatures(String sparqlQuery, String targetDataset, StorageSet storageSet) {
		
		logger.info("Processing sampling units...");
		
		List<String[]> resultSet = runQuery(sparqlQuery, null, targetDataset, 0);
		
			// Ensure the 'SampleOf' relationship type exists and if not add it
			String relationName = "SampleOf";
			LutRelationshipType relationshipType;
			if(relationshipTypeService.existsByName(relationName)) {
				relationshipType = relationshipTypeService.findByName(relationName);
			} else {
				relationshipType = new LutRelationshipType(relationName);
				relationshipTypeService.add(relationshipType);
			}
			
			// Get and store all features
			featureHelper.setStorageSet(storageSet);
			featureHelper.setFeatureIdColumn(0);
			featureHelper.setFeatureTypeColumn(1);
			featureHelper.setFeatureQualifierColumn(2);
			featureHelper.setOriginalFeatureColumn(3);
			featureHelper.setRelationshipColumn(4);
			featureHelper.setRelationshipType(relationshipType);
			featureHelper.setResultSet(resultSet);
			featureHelper.processFeatures();
			
			Iterator<String[]> resIt = resultSet.iterator();
			while(resIt.hasNext()) {
				String[] result = resIt.next();
				
				Feature feature = featureHelper.getFeature(result[0]);
				// To fix
				Procedure procedure = new Procedure(result[5]);
				procedureService.add(procedure);
				
				// Repeat for each observation on the feature
				
				LutPropertyType propertyTypeObj = (LutPropertyType) typeVocabHelper.retreiveFromLut(propertyTypeMap, propertyTypeService, LutPropertyType.class, "Area");
				LutPropertyQualifierType propertyQualifierTypeObj = (LutPropertyQualifierType) typeVocabHelper.retreiveFromLut(propertyQualifierTypeMap, propertyQualifierTypeService, LutPropertyQualifierType.class, "");
					
				Set<Result> results = new HashSet<>();
				addResult(results, "Type", "QuantMeasure", storageSet); // replace type with lookup
				addResult(results, "Value", result[6], storageSet);
//				addResult(results, "RangeLow", result[12], storageSet);
//				addResult(results, "RangeHigh", result[13], storageSet);
				addResult(results, "Standard", result[7], storageSet);
//				addResult(results, "Category", result[15], storageSet);
//				addResult(results, "Comment", result[16], storageSet);
				
				Iterator<Result> iterator = results.iterator();
				while(iterator.hasNext()) {
					resultService.add(iterator.next());
				}
				
				// Add in date stuff
				Calendar dateTime = new GregorianCalendar(); // Need to parse time
				Time time = new Time("Phen", dateTime, storageSet);
				Set<Time> times = new HashSet<>();
				times.add(time);
				timeRepository.saveAll(times);
				// Create and persist the Observation
				Observation observation = new Observation(feature, propertyTypeObj, propertyQualifierTypeObj, procedure, results, times, storageSet);
				//observation.setUltimateFeature(ultimateFeature);
				observationService.add(observation);
			}
	}
	
	private void populateStudyLocations(String sparqlQuery, String targetDataset, StorageSet storageSet) {
		logger.info("Processing study locations...");
		
		List<String[]> resultSet = runQuery(sparqlQuery, null, targetDataset, 0);
		
		// Create features
		featureHelper.setStorageSet(storageSet);
		featureHelper.setResultSet(resultSet);
		featureHelper.setFeatureIdColumn(0);
		featureHelper.setFeatureTypeColumn(1);
		featureHelper.setFeatureQualifierColumn(2);
		featureHelper.setOriginalFeatureColumn(3);
//			featureHelper.setRelationshipColumn(null);

		featureHelper.processFeatures();
		Iterator<String[]> resIt = resultSet.iterator();
		while(resIt.hasNext()) {
			String[] result = resIt.next();
			Feature feature = featureHelper.getFeature(result[0]);
//				Feature ultimateFeature = featureHelper.getFeature(result[6]);
			
			// To fix
			Procedure procedure = new Procedure(result[7]);
			procedureService.add(procedure);
			
			// Repeat for each observation on the feature
			
			LutPropertyType propertyTypeObj = (LutPropertyType) typeVocabHelper.retreiveFromLut(propertyTypeMap, propertyTypeService, LutPropertyType.class, "Identifier");
			LutPropertyQualifierType propertyQualifierTypeObj = (LutPropertyQualifierType) typeVocabHelper.retreiveFromLut(propertyQualifierTypeMap, propertyQualifierTypeService, LutPropertyQualifierType.class, "Primary");
				
			Set<Result> results = new HashSet<>();
			addResult(results, "Type", "SimpleValue", storageSet); // replace type with lookup
			addResult(results, "Value", result[5], storageSet);
			
			Iterator<Result> iterator = results.iterator();
			while(iterator.hasNext()) {
				resultService.add(iterator.next());
			}
			
//				// Add in date stuff
//				Calendar dateTime = new GregorianCalendar(); // Need to parse time
//				Time time = new Time("Phen", dateTime, storageSet);
//				Set<Time> times = new HashSet<>();
//				times.add(time);
//				timeRepository.saveAll(times);
			
			// Add in sensor stuff
//				Set<Sensor> sensors = new HashSet<Sensor>();
//				sensors.add(new Sensor("type","person", storageSet));
//				sensors.add(new Sensor("name","person", storageSet));
			
			
			// Create and persist the Observation
			Observation observation = new Observation(feature, propertyTypeObj, propertyQualifierTypeObj, procedure, results, null, storageSet);
			observation.setUltimateFeature(null);
			observationService.add(observation);
			
			// Repeat for each observation on the feature
			
			propertyTypeObj = (LutPropertyType) typeVocabHelper.retreiveFromLut(propertyTypeMap, propertyTypeService, LutPropertyType.class, "Location");
			propertyQualifierTypeObj = (LutPropertyQualifierType) typeVocabHelper.retreiveFromLut(propertyQualifierTypeMap, propertyQualifierTypeService, LutPropertyQualifierType.class, "Coordinate");
				
			results = new HashSet<>();
			addResult(results, "Type", "Coordinate", storageSet); // replace type with lookup
			addResult(results, "Datum", "GDA94", storageSet);
			addResult(results, "Longitude", result[11], storageSet);
			addResult(results, "Latitude", result[12], storageSet);
			
			iterator = results.iterator();
			while(iterator.hasNext()) {
				resultService.add(iterator.next());
			}
			
			// Create and persist the Observation
			observation = new Observation(feature, propertyTypeObj, propertyQualifierTypeObj, procedure, results, null, storageSet);
			observation.setUltimateFeature(null);
			observationService.add(observation);				
		}
	}
	
	private void populateSurveyLinks(String sparqlQuery, String targetDataset, StorageSet storageSet) {
		logger.info("Processing survey links...");
		List<String[]> resultSet = runQuery(sparqlQuery, null, targetDataset, 0);
		Iterator<String[]> iterator = resultSet.iterator();
		
		while(iterator.hasNext()) {
			String[] result = iterator.next();
			Optional<Metadata> metadata = metadataRepository.findById(result[0]);
			Optional<Feature> studyLocation = featureRepository.findById(result[2]);
			
			SurveyLink surveyLink;
			if(metadata.isPresent() && studyLocation.isPresent()) {
				surveyLink = new SurveyLink(studyLocation.get(), result[1], metadata.get());
				surveyLinkRepository.save(surveyLink);
			}else {
				System.out.println("FUCK: " + result[0] + " --- " + result[2]);
			}
		}
	}
	
	private void populateSurveyDetails(String sparqlQuery, String targetDataset, StorageSet storageSet) {
		logger.info("Processing survey stuff...");
		List<String[]> resultSet = runQuery(sparqlQuery, null, targetDataset, 0);
		Iterator<String[]> iterator = resultSet.iterator();
		
		while(iterator.hasNext()) {
			String[] result = iterator.next();
			
			Metadata metadata = new Metadata(result[0]);
			metadata.setPersistentsurveyId(result[1]);
			metadata.setSurveyName(result[2]);
			metadata.setOrganisation(result[3]);
			metadata.setCustodian(result[4]);
			
			metadata.setRights(result[5]);
			metadata.setLicence(result[6]);
			metadata.setCitation(result[7]);
			
//			Calendar modTime = new GregorianCalendar().; // Need to parse time
			metadata.setDateModified(result[8]);
			metadata.setDateAccessioned(result[9]);
			metadata.setLanguage(result[10]);
			
			metadata.setSurveyType("SurvType");
			metadata.setSurveyMethod("SurvMethod");
			metadata.setMethodLink("MethodLink");
			// Save record to database
			metadataRepository.save(metadata);
		}
	}
	
	private void populateTaxonomicObservations(String taxonomyQuery, String targetDataset, StorageSet storageSet) {
		logger.info("Processing taxonomy...");
		featureHelper.init(); // Hack to stop things exploding
		queryManager.loadQuery(taxonomyQuery);
		queryManager.replaceContent("%TARGET_DATASET%",targetDataset);
		queryManager.addLimit(10); // Set limit for testing
		List<String[]> resultSet = null;
		try {
			resultSet = queryManager.performQuery();
		} catch (Throwable e) {
			throw new RuntimeException("Failed to execute the taxonomy query", e);
		}
		
		Iterator<String[]> iterator = resultSet.iterator();
		
		while(iterator.hasNext()) {
			String[] result = iterator.next();
			
//			logger.info("Tax processing: " + result[0]);
			Feature feature = featureHelper.getFeature(result[0]);
			
			//[<http://www.aekos.org.au/ontology/1.0.0/wadec_ravensthorpe#SPECIESORGANISMGROUP-T1517616530276>,
			//<http://www.aekos.org.au/ontology/1.0.0/wadec_ravensthorpe#METHODTYPE-T1517616064874>,
			//"Name", "Determined Species Concept", "Persoonia teretifolia R.Br.", 
			//"2007-10-01T14:30:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime>]
			//logger.info("in populateTaxonomicObservations length is : " + result.length);
			//logger.info("in populateTaxonomicObservations string value : " + Arrays.toString(result));
			//Mosheh - need to capture Study Location identifier
			//Feature ultimateFeature = featureHelper.getFeature(result[6]);
			
			
			LutPropertyType propertyTypeObj = (LutPropertyType) typeVocabHelper.retreiveFromLut(propertyTypeMap, propertyTypeService, LutPropertyType.class, result[2]);
			LutPropertyQualifierType propertyQualifierTypeObj = (LutPropertyQualifierType) typeVocabHelper.retreiveFromLut(propertyQualifierTypeMap, propertyQualifierTypeService, LutPropertyQualifierType.class, result[3]);
			
			
			// To fix
			Procedure procedure = new Procedure(result[1]);
			procedureService.add(procedure);
			
			
			Set<Result> results = new HashSet<>();
			addResult(results, "Type", "QuantMeasure", storageSet); // replace type with lookup
			addResult(results, "Value", result[4], storageSet);
			
			
			Iterator<Result> resIt = results.iterator();
			while(resIt.hasNext()) {
				resultService.add(resIt.next());
			}
			
			// Add in date stuff
			Calendar dateTime = new GregorianCalendar(); // Need to parse time
			// stored on result[5];
			
			Time time = new Time("Phen", dateTime, storageSet);
			Set<Time> times = new HashSet<>();
			times.add(time);
			timeRepository.saveAll(times);
			Observation observation = new Observation(feature, propertyTypeObj, propertyQualifierTypeObj, procedure, results, times, storageSet);
//			observation.setUltimateFeature(ultimateFeature);
			observationService.add(observation);
		}
	}
	
	private void populateProceduresTable(String procedureQuery, String targetDataset) {
		
		List<Procedure> procList = procedureRepository.findByNameIsNull();
		if(!procList.isEmpty()) {
			Iterator<Procedure> it = procList.iterator();
			StringBuilder sb = new StringBuilder();
			while(it.hasNext()) {
				sb.append("<" + it.next().getProcedureId() + ">\n");
			}
			
			List<String[]> resultSet = runQuery(procedureQuery, sb.toString(), targetDataset, 0);
			
			Iterator<String[]> iterator = resultSet.iterator();
			while(iterator.hasNext()) {
				String[] result = iterator.next();
				Procedure procedure = new Procedure(result[0]);
				procedure.setName(result[1]);
				procedure.setSummary(result[2]);
				procedure.setDetails(result[3]);
				procedureRepository.save(procedure);
			}
		}
		
	}
	
	private void processHeightObservations(String queryFile, String targetDataset, StorageSet storageSet){
//		logger.info("Height processing");
		queryManager.loadQuery(queryFile);
		queryManager.replaceContent("%TARGET_DATASET%",targetDataset);
		
		queryManager.addLimit(10); // Set limit for testing
		
		
//		List<String> featureList = new ArrayList<>();

		
		List<String[]> resultSet = null;
		try {
			logger.info("Performing Height SPARQL query to Squid Linux machine.....takes a while.......");
			resultSet = queryManager.performQuery();
			logger.info("Height SPARQL query results has returned.");
		} catch (Throwable e) {
			throw new RuntimeException("Failed to execute the height observations query", e);
		}
		
		logger.info("Loading Height information now...");		
		Iterator<String[]> it = resultSet.iterator();
		while(it.hasNext()) {
			String[] result = it.next();
			
			//Example of result:
			//[<http://www.aekos.org.au/ontology/1.0.0/wadec_ravensthorpe#SPECIESORGANISMGROUP-T1517616470658>, 
			//"SpeciesOrgGroup", "Stand", "dummy", <http://www.aekos.org.au/ontology/1.0.0/wadec_ravensthorpe#SAMPLINGUNIT-T1517616470140>,
			//<http://www.aekos.org.au/ontology/1.0.0/wadec_ravensthorpe#METHODEXECUTION-T1517616470349>, 
			//<http://www.aekos.org.au/ontology/1.0.0/wadec_ravensthorpe#STUDYLOCATION-T1517616135522>, "Height",
			//"AVG", <http://www.aekos.org.au/ontology/1.0.0/wadec_ravensthorpe#METHODTYPE-T1517616064874>,
			//"2007-09-25T14:30:00Z"^^<http://www.w3.org/2001/XMLSchema#dateTime>, 0.1, , , "metres",
			//, , <http://www.aekos.org.au/ontology/1.0.0/wadec_ravensthorpe#SPECIESORGANISMGROUP-T1517616470658>]
			
			//logger.info("Expecting height row length to be 18 strings and it is: " + result.length);
			//logger.info("Processing: " + Arrays.toString(result));
			
			if (result.length < 18) {
				logger.info("Error in processHeightObservations(). String are not the expected length! Strings = " + Arrays.toString(result));
				continue;
			}
			
			Feature feature = featureHelper.getFeature(result[0]);
			
			Feature ultimateFeature = featureHelper.getFeature(result[6]);
			
			
			LutPropertyType propertyTypeObj = (LutPropertyType) typeVocabHelper.retreiveFromLut(propertyTypeMap, propertyTypeService, LutPropertyType.class, result[7]);
			LutPropertyQualifierType propertyQualifierTypeObj = (LutPropertyQualifierType) typeVocabHelper.retreiveFromLut(propertyQualifierTypeMap, propertyQualifierTypeService, LutPropertyQualifierType.class, result[8]);
			
			
			// To fix
			Procedure procedure = new Procedure(result[9]);
			procedureService.add(procedure);
			
			
			Set<Result> results = new HashSet<>();
			addResult(results, "Type", "QuantMeasure", storageSet); // replace type with lookup
			
			addResult(results, "Value", result[11], storageSet);
			addResult(results, "RangeLow", result[12], storageSet);
			addResult(results, "RangeHigh", result[13], storageSet);
			addResult(results, "Standard", result[14], storageSet);
			
			addResult(results, "Category", result[15], storageSet);
			
			addResult(results, "Comment", result[16], storageSet);
			
			
			
			Iterator<Result> iterator = results.iterator();
			while(iterator.hasNext()) {
				resultService.add(iterator.next());
			}
			
			// Add in date stuff
			Calendar dateTime = new GregorianCalendar(); // Need to parse time
			Time time = new Time("Phen", dateTime, storageSet);
			Set<Time> times = new HashSet<>();
			times.add(time);
			timeRepository.saveAll(times);
			// Create and persist the Observation
			Observation observation = new Observation(feature, propertyTypeObj, propertyQualifierTypeObj, procedure, results, times, storageSet);
			observation.setUltimateFeature(ultimateFeature);
			observationService.add(observation);
			
			// Create and persist the tempMethod object for future use
			TempMethod method = new TempMethod(result[5], observation);
			tempMethodRepository.save(method);
			
			
			// Add to feature List
//			featureList.add(feature.getFeatureId());
		}
		
//		featuresOfInterestTarget = makeTargetList(featuresOfInterest);
		logger.info("Height done");
		
		return;
	}
	
	private void addResult(Set<Result> results, String key, String value, StorageSet storageSet) {
		// check if value exists add add
		if(!value.isEmpty()) {
			results.add(new Result(key,value,storageSet));
		}
		
	}
	
	private void processBaseOrganismObservations(String queryFile, String targetDataset, StorageSet storageSet){
		logger.info("Base Organism processing");
		queryManager.loadQuery(queryFile);
		queryManager.replaceContent("%TARGET_DATASET%",targetDataset);
		
		queryManager.addLimit(10); // Set limit for testing
		
		List<String[]> resultSet = null;
		try {
			resultSet = queryManager.performQuery();
		} catch (Throwable e) {
			throw new RuntimeException("Failed to execute the height observations query", e);
		}
		
		// Ensure the 'SampleOf' relationship type exists and if not add it
		String relationName = "SampleOf";
		LutRelationshipType relationshipType;
		if(relationshipTypeService.existsByName(relationName)) {
			relationshipType = relationshipTypeService.findByName(relationName);
		} else {
			relationshipType = new LutRelationshipType(relationName);
			relationshipTypeService.add(relationshipType);
		}
		// Get and store all features
		featureHelper.setStorageSet(storageSet);
		featureHelper.setFeatureIdColumn(0);
		featureHelper.setFeatureTypeColumn(1);
		featureHelper.setFeatureQualifierColumn(3);
		featureHelper.setOriginalFeatureColumn(2);
		// featureHelper.setRelationshipColumn(5);
		featureHelper.setRelationshipColumn(4);
		featureHelper.setRelationshipType(relationshipType);
		featureHelper.setResultSet(resultSet);
		featureHelper.processFeatures();
//		featuresOfInterestTarget = makeTargetList(featuresOfInterest);
		logger.info("Base Obs done");
		return;
	}
	
	private List<String[]> runQuery(String queryFile, String targetList, String targetDataset, int limit){
		queryManager.loadQuery(queryFile);
		queryManager.replaceContent("%TARGET_DATASET%",targetDataset);
		
		if(targetList != null)
			queryManager.replaceContent("%TARGET_OBSERVATIONS%",targetList);
		
		if(limit > 0)
			queryManager.addLimit(limit); // Set limit for testing
		
		List<String[]> resultSet = null;
		try {
			resultSet = queryManager.performQuery();
		} catch (Throwable e) {
			String msg = String.format("Failed to execute query from %s", queryFile);
			throw new RuntimeException(msg, e);
		}
		return resultSet;
	}
	
	/**
	 * Mosheh
	 * @param list
	 * @param index
	 * @return
	 */
	public boolean indexExists(final String[] list, final int index) {
	    return index >= 0 && index < list.length;
	}
}
