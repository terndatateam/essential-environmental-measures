package au.org.aekos.eem.testmodel.services;

import org.springframework.stereotype.Service;

import au.org.aekos.eem.testmodel.model.StorageSet;
import au.org.aekos.eem.testmodel.repositories.StorageSetRepository;

@Service
public class StorageSetService {
	
	private StorageSetRepository storageSetRepository;

	public StorageSetService(StorageSetRepository storageSetRepository) {
		super();
		this.storageSetRepository = storageSetRepository;
	}
	
	public void addStorageSet(StorageSet storageSet) {
		storageSetRepository.save(storageSet);
	}

}
