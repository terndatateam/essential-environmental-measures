package au.org.aekos.eem.testmodel.repositories;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.SurveyLink;

public interface SurveyLinkRepository  extends CrudRepository<SurveyLink, String>{

}
