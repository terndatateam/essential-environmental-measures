package au.org.aekos.eem.testmodel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LutOriginalPropertyType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int originalPropertyTypeId; //PK
	
	private String name;
	private String description;
	private String parentConcept;
	private String link;

	public LutOriginalPropertyType() {
		// TODO Auto-generated constructor stub
	}

}
