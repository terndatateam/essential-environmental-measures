package au.org.aekos.eem.testmodel.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Feature {
	
	@Id
	private String featureId;
	
	@ManyToOne
	private LutFeatureType featureType;
	
	@ManyToOne
	private LutFeatureQualifierType featureQualifier;
	
	@ManyToOne
	private LutOriginalFeatureType originalFeatureType;
	
//	@ManyToOne
//	private 
	
	@ManyToOne
	private StorageSet storageSet;
	
//	private String dummyText;
	
	
	private Feature() {}
	
	public Feature(String featureId, StorageSet storageSet) {
		this.featureId = featureId;
		this.storageSet = storageSet;
	}
	
	public Feature(String featureId, LutFeatureType featureType, LutFeatureQualifierType featureQualifier,
			LutOriginalFeatureType originalFeatureType, StorageSet storageSet) {
//		super();
		this.featureId = featureId;
		this.featureType = featureType;
		this.featureQualifier = featureQualifier;
		this.originalFeatureType = originalFeatureType;
		this.storageSet = storageSet;
	}
	
//	private String getDummyText() {
//		if(dummyText != null)
//		return dummyText;
//		
//		return "Dummy";
//	}


	public String getFeatureId() {
//		if(featureId == null)
//			return getDummyText();
		
		return featureId;
	}


	public LutFeatureType getFeatureType() {
		return featureType;
	}


	public LutFeatureQualifierType getFeatureQualifier() {
		return featureQualifier;
	}


	public LutOriginalFeatureType getOriginalFeatureType() {
		return originalFeatureType;
	}


	public StorageSet getStorageSet() {
		return storageSet;
	}

	public void setFeatureType(LutFeatureType featureType) {
		this.featureType = featureType;
	}

	public void setFeatureQualifier(LutFeatureQualifierType featureQualifier) {
		this.featureQualifier = featureQualifier;
	}

	public void setOriginalFeatureType(LutOriginalFeatureType originalFeatureType) {
		this.originalFeatureType = originalFeatureType;
	}

	public void setStorageSet(StorageSet storageSet) {
		this.storageSet = storageSet;
	}
	
	
}
