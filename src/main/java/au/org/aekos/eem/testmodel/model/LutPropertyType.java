package au.org.aekos.eem.testmodel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LutPropertyType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long propertyTypeId; //PK
	
	private String name;
	private String description;
	private String parentConcept;
	private String link;

	public LutPropertyType() {
	}
	
	public LutPropertyType(String name) {
		super();
		this.name = name;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParentConcept() {
		return parentConcept;
	}

	public void setParentConcept(String parentConcept) {
		this.parentConcept = parentConcept;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public long getPropertyTypeId() {
		return propertyTypeId;
	}
	
	

}
