package au.org.aekos.eem.testmodel.model;

import java.sql.Blob;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Procedure {
	
	@Id
	private String procedureId; //PK
	private String name;
	@Lob
	private String summary;
	@Lob
	private String details;
	
	
	
	public Procedure() {}
	
	public Procedure(String procedureId) {
		super();
		this.procedureId = procedureId;
	}


	public String getProcedureId() {
		return procedureId;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@Override
	public String toString() {
		return "Procedure [procedureId=" + procedureId + ", name=" + name + ", summary=" + summary + ", details="
				+ details + "]";
	}




}
