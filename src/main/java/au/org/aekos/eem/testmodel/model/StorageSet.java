package au.org.aekos.eem.testmodel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StorageSet {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int storageSetId; //PK
	
	private String storageSetUri;
	
	private StorageSet() {}

	public StorageSet(String storageSetUri) {
		super();
		this.storageSetUri = storageSetUri;
	}

	public String getStorageSetUri() {
		return storageSetUri;
	}

	public void setStorageSetUri(String storageSetUri) {
		this.storageSetUri = storageSetUri;
	}

	public void setStorageSetId(int storageSetId) {
		this.storageSetId = storageSetId;
	}
	

}
