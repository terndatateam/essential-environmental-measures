package au.org.aekos.eem.testmodel.repositories;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.LutFeatureQualifierType;
import au.org.aekos.eem.testmodel.model.LutFeatureType;

public interface FeatureQualifierTypeRepository extends CrudRepository<LutFeatureQualifierType, Long>{
	
	public LutFeatureQualifierType findByName(String name);

}
