package au.org.aekos.eem.testmodel.model.helper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import au.org.aekos.eem.testmodel.model.Feature;
import au.org.aekos.eem.testmodel.model.FeatureRelationship;
import au.org.aekos.eem.testmodel.model.LutFeatureQualifierType;
import au.org.aekos.eem.testmodel.model.LutFeatureType;
import au.org.aekos.eem.testmodel.model.LutOriginalFeatureType;
import au.org.aekos.eem.testmodel.model.LutRelationshipType;
import au.org.aekos.eem.testmodel.model.StorageSet;
import au.org.aekos.eem.testmodel.repositories.FeatureRelationshipRepository;
import au.org.aekos.eem.testmodel.repositories.FeatureRepository;
import au.org.aekos.eem.testmodel.services.FeatureQualifierTypeService;
import au.org.aekos.eem.testmodel.services.FeatureService;
import au.org.aekos.eem.testmodel.services.FeatureTypeService;
import au.org.aekos.eem.testmodel.services.OriginalFeatureTypeService;

@Component
public class FeatureHelper {
	
	@Autowired
	FeatureService featureService;
//	FeatureRepository featureRepository;
	
	
	@Autowired
	private FeatureTypeService featureTypeService;
	@Autowired
	private FeatureQualifierTypeService featureQualifierTypeService;	
	@Autowired
	private OriginalFeatureTypeService originalFeatureTypeService;
	
	@Autowired
	private FeatureRelationshipRepository featureRelationshipRepository;
	
	
	@Autowired
	private TypeVocabHelper typeVocabHelper;
	
	
	
	HashMap<String, Object> featureTypeMap;
	HashMap<String, Object> featureQualifierTypeMap;
	HashMap<String, Object> originalFeatureTypeMap;
	HashMap<String, Feature> featureStore;
	
	private List<String[]> resultSet;
	private int featureIdColumn;
	private int featureTypeColumn;
	private int featureQualifierColumn;
	private int originalFeatureColumn;
	private int relationshipColumn;
	private LutRelationshipType relationshipType;
	private StorageSet storageSet;
	
	
	@Autowired
	public FeatureHelper() {}


	public Feature getFeature(String featureId) {

		if(!featureStore.containsKey(featureId)) {
			Feature feature = new Feature(featureId, storageSet);
			featureService.add(feature);
			featureStore.put(featureId, feature);
		}
		return featureStore.get(featureId);
	}
	
	public boolean processFeatures() {
		if(!checkPopulation())
			return false;
		
		init();
		
		
		
		Iterator<String[]> it = resultSet.iterator();
		while(it.hasNext()) {
			String[] result = it.next();
			Feature feature = constructFeature(result[featureIdColumn], result[featureTypeColumn], result[featureQualifierColumn], result[originalFeatureColumn]);
			
			// If relationship is populated then add relationship
//			System.out.println("Adding relationships");
			if(!result[relationshipColumn].isEmpty() && relationshipType != null) {
//				System.out.println("Adding relationship: " + result[relationshipColumn]);
				
				//Check if the target relationship feature exists and if not create it
				Feature featureTarget;
				Optional<Feature> featureTest = featureService.findById(result[relationshipColumn]);
				if(featureTest.isPresent()) {
					featureTarget = featureTest.get();
				}else {
					featureTarget = new Feature(result[relationshipColumn], storageSet);
					// Persist it
					featureService.add(featureTarget);
				}
				
//				System.out.println("Feature Target is: " + featureTarget.toString());
				
				FeatureRelationship featureRelationship = new FeatureRelationship(feature, relationshipType, featureTarget);
//				System.out.println("RElationship: " + featureRelationship.toString());
				featureRelationshipRepository.save(featureRelationship);
			}
		}
		return true;
	}
	
	private boolean checkPopulation() {
		boolean check = true;
		if(resultSet.isEmpty())
			check = false;
		if(storageSet.getStorageSetUri().isEmpty())
			check = false;
		
		return check;
	}
	
	public boolean init() {
		
		featureStore = new HashMap<String, Feature>();
		featureTypeMap = new HashMap<String, Object>();
		featureQualifierTypeMap = new HashMap<String, Object>();
		originalFeatureTypeMap = new HashMap<String, Object>();
		
		return true;
	}
	
	

	public Feature constructFeature(String featureId, String featureType, String featureQualifier, String originalFeature) {
		
		LutFeatureType featureTypeObj = (LutFeatureType) typeVocabHelper.retreiveFromLut(featureTypeMap, featureTypeService, LutFeatureType.class, featureType);
		LutFeatureQualifierType featureQualifierTypeObj = (LutFeatureQualifierType) typeVocabHelper.retreiveFromLut(featureQualifierTypeMap, featureQualifierTypeService, LutFeatureQualifierType.class, featureQualifier);
		LutOriginalFeatureType originalFeatureTypeObj = (LutOriginalFeatureType) typeVocabHelper.retreiveFromLut(originalFeatureTypeMap, originalFeatureTypeService, LutOriginalFeatureType.class, originalFeature);
		
		Feature feature = new Feature(featureId, featureTypeObj, featureQualifierTypeObj, originalFeatureTypeObj, storageSet);
		featureService.add(feature);
		
		featureStore.put(featureId, feature);
		
		return feature;
	}



	// Getters and setters

	public List<String[]> getResultSet() {
		return resultSet;
	}

	public void setResultSet(List<String[]> resultSet) {
		this.resultSet = resultSet;
	}

	public int getFeatureIdColumn() {
		return featureIdColumn;
	}

	public void setFeatureIdColumn(int featureIdColumn) {
		this.featureIdColumn = featureIdColumn;
	}

	public int getFeatureTypeColumn() {
		return featureTypeColumn;
	}

	public void setFeatureTypeColumn(int featureTypeColumn) {
		this.featureTypeColumn = featureTypeColumn;
	}

	public int getFeatureQualifierColumn() {
		return featureQualifierColumn;
	}

	public void setFeatureQualifierColumn(int featureQualifierColumn) {
		this.featureQualifierColumn = featureQualifierColumn;
	}

	public int getOriginalFeatureColumn() {
		return originalFeatureColumn;
	}

	public void setOriginalFeatureColumn(int originalFeatureColumn) {
		this.originalFeatureColumn = originalFeatureColumn;
	}

	public int getRelationshipColumn() {
		return relationshipColumn;
	}

	public void setRelationshipColumn(int relationshipColumn) {
		this.relationshipColumn = relationshipColumn;
	}

	public LutRelationshipType getRelationshipType() {
		return relationshipType;
	}

	public void setRelationshipType(LutRelationshipType relationshipType) {
		this.relationshipType = relationshipType;
	}

	public StorageSet getStorageSet() {
		return storageSet;
	}

	public void setStorageSet(StorageSet storageSet) {
		this.storageSet = storageSet;
	}

}
