package au.org.aekos.eem.testmodel.services;

public abstract class AbstractService {
	
	public abstract void add(Object object);
	public abstract Object findByName(String name);
	public abstract boolean existsByName(String name);
}
