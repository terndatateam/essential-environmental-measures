package au.org.aekos.eem.testmodel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LutRelationshipType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int featureTypeId; //PK
	
	private String name;
	private String description;
		
	private LutRelationshipType() {}

	public LutRelationshipType(String name) {
		super();
		this.name = name;
	}

	public LutRelationshipType(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getFeatureTypeId() {
		return featureTypeId;
	}
	
	

}
