package au.org.aekos.eem.testmodel.repositories;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.LutOriginalFeatureType;

public interface OriginalFeatureTypeRepository extends CrudRepository<LutOriginalFeatureType, Long>{

	public LutOriginalFeatureType findByName(String name);

}
