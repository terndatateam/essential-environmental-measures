package au.org.aekos.eem.testmodel.repositories;

import org.springframework.data.repository.CrudRepository;

import au.org.aekos.eem.testmodel.model.TempMethod;

public interface TempMethodRepository extends CrudRepository<TempMethod, String>{

}
