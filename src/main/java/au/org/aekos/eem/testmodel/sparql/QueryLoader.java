package au.org.aekos.eem.testmodel.sparql;


import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class QueryLoader {
	
	private static String endPoint;

	public QueryLoader(String endpoint) {
		QueryLoader.endPoint = endpoint;
	}

	protected String getQuery(String fileName) {
		InputStream sparqlIS = Thread.currentThread().getContextClassLoader().getResourceAsStream("sparql/" + fileName);
		OutputStream out = new ByteArrayOutputStream();
		try {
			copy(sparqlIS, out);
			return out.toString();
		} catch (IOException e) {
			throw new RuntimeException("Failed to read file " + fileName, e);
		}
	}
	
	protected List<String[]> performQuery(String query) throws Throwable {
		String urlEncodedQuery = URLEncoder.encode(query, "UTF-8");
		try {
			final boolean nobodyLikesYouHeader = true;
			List<String[]> result = postForCsv("query=" + urlEncodedQuery, nobodyLikesYouHeader);
//			for (String[] curr : csv) {
//				System.out.println(Arrays.toString(curr));
//			}
			return result;
		} catch (Exception e) {
			throw new RuntimeException("Failed to perform query", e);
		}
	}
	
	protected List<String> performQuerySimple(String query) throws Throwable {
		List<String[]> complex = performQuery(query);
		
		List<String> result = new LinkedList<>();
		Iterator<String[]> iterator = complex.iterator();
		while(iterator.hasNext()){
			result.add(iterator.next()[0]);
		}
		
		return result;
	}
	
	private static final int BUFFER_SIZE = 4096;
	private static int copy(InputStream in, OutputStream out) throws IOException {
		int byteCount = 0;
		byte[] buffer = new byte[BUFFER_SIZE];
		int bytesRead = -1;
		while ((bytesRead = in.read(buffer)) != -1) {
			out.write(buffer, 0, bytesRead);
			byteCount += bytesRead;
		}
		out.flush();
		return byteCount;
	}
	
	private List<String[]> postForCsv(String formData, boolean isDiscardHeader) throws Exception {
//		final String acceptMime = "application/sparql-results+json"; // JSON
		final String acceptMime = "text/csv"; // CSV
		List<String[]> result = new LinkedList<>();
		URL obj = new URL(endPoint);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		//add request header
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept", acceptMime);
		con.setRequestProperty("Accept-Language", "en-AU,en-GB;q=0.8,en-US;q=0.6,en;q=0.4");
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(formData);
		wr.flush();
		wr.close();
		int responseCode = con.getResponseCode();
		boolean isSuccess = responseCode == 200;
		if (!isSuccess) {
			throw new RuntimeException("Failed to perform query. HTTP status code = " + responseCode);
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		boolean isFirst = true;
		while ((inputLine = in.readLine()) != null) {
			if (isDiscardHeader && isFirst) {
				isFirst = false;
				continue;
			}
			result.add(inputLine.split(","));
		}
		in.close();
		return result;
	}
}
