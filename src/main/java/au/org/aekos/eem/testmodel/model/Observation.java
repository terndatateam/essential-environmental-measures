package au.org.aekos.eem.testmodel.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ManyToAny;

@Entity
public class Observation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long observationId; //PK
	
	@ManyToOne
	private Feature feature;
	
	@ManyToOne
	private Feature ultimateFeature;
	
	@ManyToOne
	private LutPropertyType property;
	
	@ManyToOne
	private LutPropertyQualifierType propertyQualifier;
	
//	@OneToOne
//	private OriginalPropertyType originalProperty;
	
	@ManyToOne
	private Procedure procedure;
	
	@OneToMany
	private Set<Result> results;	
	
	@OneToMany
	private Set<Time> times;

	@ManyToOne
	private StorageSet storageSet;
	
//	@OneToMany
//	private Set<Value> sensors = new HashSet();
	
	public Observation() {}

	public Observation(Feature feature, LutPropertyType property, LutPropertyQualifierType propertyQualifier,
			Procedure procedure, Set<Result> values, Set<Time> times, StorageSet storageSet) {
		super();
		this.feature = feature;
		this.property = property;
		this.propertyQualifier = propertyQualifier;
		this.procedure = procedure;
		this.results = values;
		this.times = times;
		this.storageSet = storageSet;
		this.ultimateFeature = null;
		
	}

	public Feature getFeature() {
		return feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public LutPropertyType getProperty() {
		return property;
	}

	public void setProperty(LutPropertyType property) {
		this.property = property;
	}

	public LutPropertyQualifierType getPropertyQualifier() {
		return propertyQualifier;
	}

	public void setPropertyQualifier(LutPropertyQualifierType propertyQualifier) {
		this.propertyQualifier = propertyQualifier;
	}

	public Procedure getProcedure() {
		return procedure;
	}

	public void setProcedure(Procedure procedure) {
		this.procedure = procedure;
	}

	public StorageSet getStorageSet() {
		return storageSet;
	}

	public void setStorageSet(StorageSet storageSet) {
		this.storageSet = storageSet;
	}

	public Set<Result> getValues() {
		return results;
	}

	public void setValues(Set<Result> values) {
		this.results = values;
	}

	public long getObservationId() {
		return observationId;
	}

	public Feature getUltimateFeature() {
		return ultimateFeature;
	}

	public void setUltimateFeature(Feature ultimateFeature) {
		this.ultimateFeature = ultimateFeature;
	}


	

}
