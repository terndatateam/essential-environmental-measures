package au.org.aekos.eem.testmodel.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class TempMethod {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long tempMethodId; //PK

	private String methodInstance;
	
	@OneToOne
	private Observation observations;

	public TempMethod(String methodInstance, Observation observations) {
		super();
		this.methodInstance = methodInstance;
		this.observations = observations;
	}

	public String getMethodInstance() {
		return methodInstance;
	}

	public void setMethodInstance(String methodInstance) {
		this.methodInstance = methodInstance;
	}

	public Observation getObservations() {
		return observations;
	}

	public void setObservations(Observation observations) {
		this.observations = observations;
	}

	public long getTempMethodId() {
		return tempMethodId;
	}


	
}
