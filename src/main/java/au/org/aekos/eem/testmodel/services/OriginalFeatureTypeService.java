package au.org.aekos.eem.testmodel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.org.aekos.eem.testmodel.model.LutFeatureType;
import au.org.aekos.eem.testmodel.model.LutOriginalFeatureType;
import au.org.aekos.eem.testmodel.repositories.OriginalFeatureTypeRepository;

@Service
public class OriginalFeatureTypeService extends AbstractService{
	
	OriginalFeatureTypeRepository originalFeatureTypeRepository;

	@Autowired
	public OriginalFeatureTypeService(OriginalFeatureTypeRepository originalFeatureTypeRepository) {
		super();
		this.originalFeatureTypeRepository = originalFeatureTypeRepository;
	}
	
//	public void addOriginalFeatureType(OriginalFeatureType originalFeatureType) {
//		originalFeatureTypeRepository.save(originalFeatureType);
//	}

	@Override
	public void add(Object object) {
		originalFeatureTypeRepository.save((LutOriginalFeatureType) object);
		
	}

	@Override
	public Object findByName(String name) {
		return originalFeatureTypeRepository.findByName(name);
	}
	
	@Override
	public boolean existsByName(String name) {
		LutOriginalFeatureType ft = originalFeatureTypeRepository.findByName(name);
		
		if(ft.getName().isEmpty())
			return false;
		return true;
	}

}
