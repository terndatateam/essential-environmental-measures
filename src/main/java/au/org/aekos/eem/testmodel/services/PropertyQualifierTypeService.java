package au.org.aekos.eem.testmodel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.org.aekos.eem.testmodel.model.LutFeatureType;
import au.org.aekos.eem.testmodel.model.LutPropertyQualifierType;
import au.org.aekos.eem.testmodel.repositories.PropertyQualifierTypeRepository;

@Service
public class PropertyQualifierTypeService extends AbstractService{
	
	private PropertyQualifierTypeRepository propertyQualifierTypeRepository;
	
	@Autowired
	public PropertyQualifierTypeService(PropertyQualifierTypeRepository propertyQualifierTypeRepository) {
		super();
		this.propertyQualifierTypeRepository = propertyQualifierTypeRepository;
	}

	@Override
	public void add(Object object) {
		propertyQualifierTypeRepository.save((LutPropertyQualifierType) object);
	}

	@Override
	public LutPropertyQualifierType findByName(String name) {
		return propertyQualifierTypeRepository.findByName(name);
	}

	@Override
	public boolean existsByName(String name) {
		LutPropertyQualifierType ft = propertyQualifierTypeRepository.findByName(name);
		
		if(ft.getName().isEmpty())
			return false;
		return true;
	}
}
