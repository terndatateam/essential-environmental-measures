#!/bin/bash
cd `dirname "$0"`
pass=$1
if [ -z "$pass" ]; then
  echo "[ERROR] you need to pass the DB password as the first arg"
  echo "usage: $0 <db-pass>"
  echo "   eg: $0 somepass"
  exit 1
fi
./mvnw spring-boot:run -D--spring.datasource.password=$pass

